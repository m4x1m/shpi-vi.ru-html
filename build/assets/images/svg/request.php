<?php

use Bitrix\Main;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

Main\Loader::includeModule('iblock');

$iblockId = 42;
$request = Main\Context::getCurrent()->getRequest();

if (!isset($_SESSION['CIRCLE_ID']))
{
	$params = [];

	if (!empty($_GET))
	{
		foreach ($_GET as $k => $v)
		{
			$params[] = $k . '=' . $v;
		}
	}

	$_SESSION['CIRCLE_ID'] = md5(time());
	(new CIBlockElement)->Add([
		'IBLOCK_SECTION_ID' => false,
		'IBLOCK_ID' => $iblockId,
		'NAME' => $_SESSION['CIRCLE_ID'],
		'PROPERTY_VALUES' => [
			'PARAMS' => $params
		],
		'ACTIVE' => 'N',
	]);
}


if ($request->getRequestMethod() === 'POST' && $request->get('AJAX_CALL') === 'Y')
{
	$arItem = CIBlockElement::GetList(
		[],
		[
			'IBLOCK_ID' => $iblockId,
			'NAME' => $_SESSION['CIRCLE_ID']
		]
	)->Fetch();

	$arProp = [];
	$arSteps = explode(',', $request->get('STEP'));
	if (!empty($arSteps))
	{
		foreach ($arSteps as $k => $prop)
		{
			$arProp['STEP_' . ($k + 1)] = $prop;
		}
	}

	$clearSession = false;
	foreach (['name', 'tel', 'email'] as $field)
	{
		if (!empty($request->get($field)))
		{
			$clearSession = true;
			$arProp[strtoupper($field)] = $request->get($field);
		}
	}

	CIBlockElement::SetPropertyValuesEx(
		$arItem['ID'],
		$iblockId,
		$arProp
	);

	if ($clearSession === true)
	{
		/**
		 * Отправим данные в б24
		 */
		$arFields = [
			'NAME' => $arProp['NAME'],
			'PHONE' => $arProp['TEL'],
			'EMAIL' => $arProp['EMAIL'],
			'REFER' => $request->get('REFERER'),
			'TITLE' => $request->get('TITLE'),
			'STEP' => [
				$arProp['STEP_1'],
				$arProp['STEP_2'],
				$arProp['STEP_3'],
				$arProp['STEP_4'],
				$arProp['STEP_5'],
				$arProp['STEP_6'],
			]
		];

		sendToBitrix24Cloud($iblockId, $arFields);

		/**
		 * Очистим сессию
		 */
		unset($_SESSION['CIRCLE_ID']);
	}

	CMain::FinalActions('ok');
	die();
}
class ShowMore {
    constructor () {
        this.more          = 'js-show-more';
        this.buttonClass   = 'js-show-more-button';
        this.buttons       = document.querySelectorAll(`.${this.buttonClass}`);
        this.expandedClass = '_expanded';

        this.init();
    }

    expandContent(parent){
        if ( !parent.classList.contains(this.expandedClass) ) {
            parent.classList.add(this.expandedClass);
        }
        else {
            parent.classList.remove(this.expandedClass);
        }
    }
    changeText(parent, element){
        const expanded  = element.getAttribute('data-expanded');
        const collapsed = element.getAttribute('data-collapsed');
        if ( !parent.classList.contains(this.expandedClass) ) {
            element.innerText = collapsed;
        }
        else {
            element.innerText = expanded;
        }
    }
    yandexBrowserDetect(){
        let isYandexBrowser = false;
        if ( navigator.userAgent.search(/YaBrowser/) > 0 ) {
            isYandexBrowser = true;
        }
        return isYandexBrowser
    }
    toggleMore() {
        const array = [...this.buttons];
        array.forEach(element => {
            element.addEventListener('click',()=>{
                const parent = findAncestor(element, this.more);

                this.expandContent(parent);
                this.changeText(parent, element);

                if ( this.yandexBrowserDetect() ) {
                    window.scrollTo({
                        top      : parent.offsetTop - 100,
                        left     : 0,
                        behavior : 'smooth'
                    });
                }
            });
        });
    }
    init() {
        this.toggleMore();
    }
}
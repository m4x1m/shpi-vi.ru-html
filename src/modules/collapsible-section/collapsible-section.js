class CollapsibleSection {
    constructor() {
        this.parent         = 'js-collapsible-section';
        this.buttons        = document.getElementsByClassName('js-collapsible-section-button');
        this.expandedClass  = 'collapsible-section--expanded';

        this.init();
    }

    toggleHandler(parent) {
        if ( !parent.classList.contains(this.expandedClass) ) {
            parent.classList.add(this.expandedClass);
        }
        else {
            parent.classList.remove(this.expandedClass);
        }
    }
    bindEvent() {
        const array = [...this.buttons];
        array.forEach(element => {
            element.addEventListener('click',()=>{
                const parent = findAncestor(element, this.parent);

                this.toggleHandler(parent);
            });
        });
    }
    init() {
        this.bindEvent();
    }
}
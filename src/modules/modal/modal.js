const modal = (id) => {
    if ( !id.length )
        return false;

    const ID = id.attr('href').substring(1);

    if ( document.getElementById(ID) === null )
        return false;

    id.magnificPopup({
        type            :'inline',
        removalDelay    : 200,
        midClick        : true,
        callbacks       : {
            beforeOpen  : function() {
                this.st.mainClass = this.st.el.attr('data-effect');
            },
            open        : function () {
                setWidth();
                if ( typeof this.st.el.attr('data-slider-updater') !== 'undefined' ) {
                    swiperUpdateHandler(this.st.el.attr('data-slider-updater'));
                }
            },
            close       : function () {
                unsetWidth();
            }
        }
    });
};

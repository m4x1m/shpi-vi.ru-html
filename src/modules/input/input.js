class InputHandler {
    constructor() {
        this.inputClass        = 'js-input';
        this.inputFieldClass   = 'js-input-field';
        this.resetClass        = '.js-input-reset';
        this.removeClass       = '.js-input-remove';
        this.focusedClass      = 'input--focused';
        this.notEmptyClass     = 'input--not-empty';
        this.successClass      = 'input--success';
        this.errorClass        = 'input--error';
        this.dropdownClass     = 'input--dropdowned';
        this.dropdownElement   = '.js-input-dropdown-element';
        this.allowClickOutside = false;

        this.init();
    }

    toggleFocus(parent){
        if ( !parent.classList.contains(this.focusedClass) ) {
            parent.classList.add(this.focusedClass);
        }
    }
    toggleBlur(parent, element) {
        if ( parent.classList.contains(this.focusedClass) && !element.value ) {
            parent.classList.remove(this.focusedClass);
        }
    }
    toggleNotEmpty(parent, element) {
        if ( element.value != '' ) {
            parent.classList.add(this.notEmptyClass);
        }
        else {
            parent.classList.remove(this.notEmptyClass);

            if ( parent.classList.contains(this.successClass) ) {
                parent.classList.remove(this.successClass);
            }
        }
    }
    removeValueHandler(parent, element) {
        if ( element.value !== '' ) {
            this.resetHandler(parent, element);
        }
    }
    showDropdownHandler(parent, element) {
        //if ( element.value.length > 2 ) {
            parent.classList.add(this.dropdownClass);

            this.allowClickOutside = true;
        //}
    }
    hideDropdownHandler(parent, element){
        if ( parent.classList.contains(this.dropdownClass) ) {
            parent.classList.remove(this.dropdownClass);
        }
    }
    setInputValue(parent, element, value) {
        element.value = value;
        this.hideDropdownHandler(parent, element);
    }
    resetHandler(parent, element) {
        parent.classList.remove(this.notEmptyClass, this.errorClass, this.successClass);
        element.value = '';
        this.hideDropdownHandler(parent, element);
    }
    clickOutside(data) {
        let isClickInside = data.element.contains(data.event.target);
        if (!isClickInside && data.flag) {
            data.parent.classList.remove(this.dropdownClass);
        }
    }

    bindEvents() {
        const inputField = document.querySelectorAll(`.${this.inputFieldClass}`);
        inputField.forEach(element => {
            const parent = findAncestor(element, this.inputClass);
            const reset  = parent.querySelector(this.resetClass);
            const remove = parent.querySelector(this.removeClass);
            const location = parent.getAttribute('data-location');
            const options = [...parent.querySelectorAll(this.dropdownElement)];

            element.addEventListener('focus',()=>{
                this.toggleFocus(parent);

                if ( location !== null ) {
                    this.removeValueHandler(parent, element)
                }
            });
            element.addEventListener('blur',()=>{
                //this.toggleBlur(parent);
                this.toggleBlur(parent, element);
            });
            element.addEventListener('keyup',()=>{
                this.toggleNotEmpty(parent, element);

                if ( location !== null ) {
                    this.showDropdownHandler(parent, element);
                }
            });

            options.forEach(option => {
                option.addEventListener('click', ()=>{
                    this.setInputValue(parent, element, option.getAttribute('data-value'));
                });
            });

            if ( reset !== null ) {
                reset.addEventListener('click', ()=>{
                    this.resetHandler(parent, element);
                });
            }
            if ( remove !== null ) {
                remove.addEventListener('click', ()=>{
                    this.resetHandler(parent, element);
                });
            }
            document.addEventListener("DOMContentLoaded", this.toggleNotEmpty(parent, element));

            //hide dropdown on click outside
            if ( location !== null ) {
                document.addEventListener('click', (e)=>{
                    this.clickOutside({parent: parent, element: element, flag: this.allowClickOutside, event: e });
                });
            }
        });
    }
    init () {
        this.bindEvents();
    }
}
const textAreaAutoHeight = () => {
    const PARAMS = {
        textarea : document.querySelectorAll('textarea'),
        setHeightHandler(element) {
            if ( element.getAttribute('data-height') !== null ) {
                element.style.height = `${parseInt(element.getAttribute('data-height'))}px`
            }
        },
        handler() {
            const el = this;
            setTimeout(function(){
                el.style.cssText = 'height:' + el.scrollHeight + 'px';

                if ( el.getAttribute('data-height') === null ) {
                    el.setAttribute('data-height', el.scrollHeight);
                }
                else if ( el.getAttribute('data-height') != el.scrollHeight ) {
                    el.setAttribute('data-height', el.scrollHeight);
                }

                if (el.value === '') {
                    el.removeAttribute('style');
                    el.removeAttribute('data-height');
                }
            }, 0);
        }
    };
    PARAMS.textarea.forEach(element => {
        element.addEventListener('keydown', PARAMS.handler);
        PARAMS.setHeightHandler(element);
    });
};

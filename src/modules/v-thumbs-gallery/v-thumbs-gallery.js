const vThumbsGallery = () => {

    const PARAMS = {
        thumbs : '.js-v-thumbs-gallery-thumbs',
        slider : '.js-v-thumbs-gallery'
    };

    if ( $(PARAMS.thumbs).length && $(PARAMS.slider).length ) {
        let gallery = [];
        let thumbs  = [];

        $(PARAMS.thumbs).each(function(i){
            const _this = $(this);
            thumbs[i] = new Swiper(_this, {
                direction             :'vertical',
                spaceBetween          : 8,
                slidesPerView         : 'auto',
                watchSlidesVisibility : true,
                watchSlidesProgress   : true
            });

            return thumbs;
        });
        $(PARAMS.slider).each(function(i){
            const _this = $(this);

            gallery[i] = new Swiper(_this, {
                spaceBetween : 0,
                navigation: {},
                pagination: {
                    el : '.js-v-thumbs-gallery-pagination',
                    clickable : true
                },
                thumbs: {
                    swiper: thumbs[i]
                },
            });
        });
    }
};
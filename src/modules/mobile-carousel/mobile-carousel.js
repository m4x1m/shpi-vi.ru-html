const mobileCarousel = () => {
    const PARAMS = {
        wrapper   : [...document.querySelectorAll('.js-mobile-carousel-wrapper')],
        slider	  : '.js-mobile-carousel',
        dots      : '.js-mobile-carousel-pagination',
        enableCarouse(){
            if ( window.deviceWidth <= WIDTH._767 ) {
                return true;
            }
        }
    };

    if ( PARAMS.wrapper.length ) {
        let swiperSlider = [];

        PARAMS.wrapper.forEach((element, index) => {
            const slider = element.querySelector(PARAMS.slider);
            const dots   = element.querySelector(PARAMS.dots);

            if ( PARAMS.enableCarouse() ) {
                swiperSlider[index] = new Swiper(slider, {
                    slidesPerView	: 'auto',
                    loop			: false,
                    autoHeight      : false,
                    centeredSlides	: false,
                    spaceBetween    : 0,
                    pagination: {
                        el 		  : dots,
                        clickable : true
                    }
                });
            }
        });
    }
};
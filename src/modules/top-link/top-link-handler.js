const topLinkHandler = () => {
    let visible = false;
    const PARAMS = {
        link : document.querySelector('.js-top-link'),
        visibleClass : '_top-link-is-visible',

        getScreenHeightHandler(){
            return window.innerHeight
        },
        getScrollTopHandler(){
            return window.pageYOffset;
        },
        getFooterOffsetTopHandler() {
            return APP.websiteFooter.offsetTop;
        },
        footerVisibilityHandler(){
            if ( PARAMS.getScreenHeightHandler() + PARAMS.getScrollTopHandler() > PARAMS.getFooterOffsetTopHandler() ) {
                visible = true
            }
            else {
                visible = false
            }

            return visible;
        },
        visibilityHandler(){
            if ( PARAMS.footerVisibilityHandler() ) {
                APP.body.classList.add(PARAMS.visibleClass);
            }
            else {
                APP.body.classList.remove(PARAMS.visibleClass);
            }
        },
        scrollTopHandler(){
            $('html, body').animate({
                scrollTop:0
            }, '500');
        }
    };

    if ( PARAMS.link === null )
        return false;

    PARAMS.link.addEventListener('click', ()=>{
        PARAMS.scrollTopHandler();
    });
    document.addEventListener('scroll', ()=>{
        PARAMS.visibilityHandler();
    }, {passive: true});
};
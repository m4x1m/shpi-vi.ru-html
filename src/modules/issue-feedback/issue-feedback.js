class IssueFeedback {
    constructor() {
        this.block = [...document.querySelectorAll('.js-issue-feedback')];
        this.buttonClass = '.js-issue-feedback-control';
        this.expandedClass = 'issue-feedback--expanded';
        this.successClass = 'issue-feedback--success';
        this.resetClass = '.js-issue-feedback-reset';

        this.inputClass = '.js-input';
        this.inputNotEmptyClass = 'input--not-empty';

        this.Init();
    }

    ExpandHandler(block){
        block.classList.add(this.expandedClass);
    }
    CollapseHandler(block, input){
        block.classList.remove(this.expandedClass);

        if ( typeof input !== 'undefined' ) {
            if ( input.classList.contains(this.inputNotEmptyClass) ) {
                input.classList.remove(this.inputNotEmptyClass)
            }
        }
    }
    SuccessHandler(element){
        element.classList.add(this.successClass);
    }
    BindEvent(){
        this.block.forEach(block => {
            const buttons = [...block.querySelectorAll(this.buttonClass)];
            const reset   = block.querySelector(this.resetClass);
            const input   = block.querySelector(this.inputClass);

            buttons.forEach(button =>{
                button.addEventListener('click', ()=>{
                    if ( button.getAttribute('data-type') === 'true' ) {
                        this.SuccessHandler(block);
                    }
                    else if ( button.getAttribute('data-type') === 'false' ) {
                        this.ExpandHandler(block);
                    }
                });
            });
            reset.addEventListener('click', ()=>{
                this.CollapseHandler(block, input);
            });
        });
    }
    Init(){
        this.BindEvent()
    }
}
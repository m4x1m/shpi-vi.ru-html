const hThumbsGallery = () => {

    const PARAMS = {
        thumbs : '.js-h-thumbs-gallery-thumbs',
        slider : '.js-h-thumbs-gallery-slider',
        pager  : '.js-h-thumbs-gallery-slider-pagination',
        prev   : '.js-h-thumbs-gallery-slider-arrow-prev',
        next   : '.js-h-thumbs-gallery-slider-arrow-next',
    };

    if ( $(PARAMS.thumbs).length && $(PARAMS.slider).length ) {
        const galleryThumbs = new Swiper(PARAMS.thumbs, {
            direction             :'horizontal',
            spaceBetween          : 8,
            slidesPerView         : 'auto',
            freeMode              : true,
            watchSlidesVisibility : true,
            watchSlidesProgress   : true
        });
        const gallery = new Swiper(PARAMS.slider, {
            spaceBetween : 0,
            autoHeight   : true,
            navigation: {
                prevEl: PARAMS.prev,
                nextEl: PARAMS.next,
            },
            pagination: {
               el : PARAMS.pager,
               clickable : true
            },
            thumbs: {
                swiper: galleryThumbs
            },
            breakpoints: {
                768: {}
            }
        });
    }
};
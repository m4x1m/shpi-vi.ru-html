class Counter {
    constructor(){
        this.counter = 'js-counter';
        this.remove  = 'js-counter-remove';
        this.add     = 'js-counter-add';
        this.input   = 'js-counter-input';

        this.init();
    }
    decrement(parent, element) {
        const min = parseInt(parent.getAttribute('data-min'));
        const input = parent.getElementsByClassName(this.input)[0];
        let currentValue = parseInt(input.value);

        if ( !isNaN(currentValue) && currentValue > min ) {
            parent.getElementsByClassName(this.add)[0].disabled = false;
            input.value = currentValue - 1
        }
        else {
            element.disabled = true;
            input.value = min;
        }
    }
    increment(parent, element) {
        const max = parseInt(parent.getAttribute('data-max'));
        const input = parent.getElementsByClassName(this.input)[0];
        let currentValue = parseInt(input.value);

        if ( !isNaN(currentValue) && currentValue < max ) {
            parent.getElementsByClassName(this.remove)[0].disabled = false;
            input.value = currentValue + 1;
        }
        else {
            element.disabled = true;
            input.value = max;
        }
    }
    bindEvents() {
        const remove = document.querySelectorAll(`.${this.remove}`);
        const add    = document.querySelectorAll(`.${this.add}`);

        remove.forEach(element => {
            element.addEventListener('click',()=>{
                const parent = findAncestor(element, this.counter);

                this.decrement(parent, element);
            });
        });
        add.forEach(element => {
            element.addEventListener('click',()=>{
                const parent = findAncestor(element, this.counter);

                this.increment(parent, element);
            });
        });
    }

    init() {
        this.bindEvents();
    }
}
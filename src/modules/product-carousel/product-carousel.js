const productCarousel = () => {
    const PARAMS = {
        slider      : $('.js-product-carousel'),
        thumbs      : $('.js-product-carousel-thumbs'),
    };
    if ( PARAMS.slider.length && PARAMS.thumbs.length ) {

        PARAMS.slider.on('init', function(event, slick){
            PARAMS.slider.removeClass(APP.hiddenClass);
            PARAMS.thumbs.removeClass(APP.hiddenClass);
        });

        PARAMS.slider.slick({
            slidesToShow    : 1,
            arrows          : false,
            dots            : false,
            infinite        : false,
            //vertical        : true,
            asNavFor        : PARAMS.thumbs,
            //draggable       : false,
            responsive      : [
                {
                    breakpoint: 1199,
                    settings: {
                        vertical : false
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        arrows   : true,
                        dots     : true,
                        vertical : false
                    }
                }
            ]
        });
        PARAMS.thumbs.slick({
            slidesToShow    : 5,
            arrows          : false,
            dots            : false,
            infinite        : true,
            vertical        : true,
            centerMode      : true,
            centerPadding   : '9.6rem 0',
            focusOnSelect   : true,
            asNavFor        : PARAMS.slider,
            draggable       : false,
            responsive      : [
                {
                    breakpoint: 1199,
                    settings: {
                        slidesToShow  : 1,
                        variableWidth : true,
                        centerMode    : false,
                        vertical      : false
                    }
                }
            ]
        });
    }
};
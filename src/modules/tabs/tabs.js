class Tabs {
    constructor() {
        this.tabs        = 'js-tabs';
        this.buttonClass = 'js-tabs-button';
        this.paneClass   = 'js-tabs-pane';
        this.buttons     = document.querySelectorAll(`.${this.buttonClass}`);
        this.activeClass = '_active';

        this.trigger     = [...document.querySelectorAll('.js-tabs-trigger')];
        this.offsetCorrector = (window.deviceWidth <= WIDTH._767 ? 20 : 70);

        this.init();
    }
    enableTab(element){
        const id  = element.getAttribute('data-tab-id');
        const tab = document.querySelector(`[data-tab="${id}"]`);

        element.classList.add(this.activeClass);
        tab.classList.add(this.activeClass);
    }
    disableTabs(parent){
        const buttons = [...parent.querySelectorAll(`.${this.buttonClass}`)];
        const tabs    = [...parent.querySelectorAll(`.${this.paneClass}`)];
        buttons.forEach(element => {
            element.classList.remove(this.activeClass);
        });
        tabs.forEach(element => {
            element.classList.remove(this.activeClass);
        });
    }
    toggleTabs(){
        const array = [...this.buttons];
        array.forEach(element => {
            element.addEventListener('click',()=>{
                const parent = findAncestor(element, this.tabs);

                this.disableTabs(parent);
                this.enableTab(element);
                this.setURL(element);
            });
        });
    }
    setURL(element) {
        const url = element.getAttribute('data-tab-id');
        //window.location.hash = url;
        window.history.replaceState(undefined, undefined, `#${url}`)
    }
    toggleHashTab() {
        if ( window.location.hash ) {
            if ( document.querySelector(`[data-tab-id="${window.location.hash.substring(1)}"]`) != null ) {
                setTimeout(()=>{
                    document.querySelector(`[data-tab-id="${window.location.hash.substring(1)}"]`).click();
                }, 350)
            }
        }
    }
    scrollToTabHandler(){
        this.trigger.forEach(trigger => {
            trigger.addEventListener('click', ()=>{
                const tabButton = document.querySelector('[data-tab-id="'+ trigger.getAttribute('data-tab-trigger') +'"]');
                if ( tabButton === null ) return false;

                tabButton.click();
                $('html, body').animate({
                    scrollTop: tabButton.offsetTop - this.offsetCorrector
                }, 600);
            });
        });
    }
    bindEvents(){
        this.toggleTabs();
        this.toggleHashTab();
        this.scrollToTabHandler();
    }

    init() {
        this.bindEvents();
    }
}
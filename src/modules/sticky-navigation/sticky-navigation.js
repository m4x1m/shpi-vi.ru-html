/*
* Прилипание категорий происходит,
* когда последний пункт доходит до верха экрана,
* вне зависимости от кол-ва пунктов
*/

class StickyNavigation {
    constructor() {
        this.parent           = document.getElementById('js-sticky-navigation-parent');
        this.wrapper          = document.getElementById('js-sticky-navigation-wrapper');
        this.navigation       = document.getElementById('js-sticky-navigation');
        this.elements         = document.querySelectorAll('.js-sticky-navigation-element');
        this.more             = document.getElementById('js-sticky-navigation-more');
        this.anchor           = document.getElementById('js-sticky-navigation-anchor');
        this.websiteHeader    = document.getElementById('js-website-header');
        this.websiteBar       = document.querySelector('.website-bar');
        this.stickyClass      = 'sticky-navigation--sticky';
        this.initClass        = 'sticky-navigation-wrapper--initialized';
        this.hiddenClass      = 'sticky-navigation__element--hidden';
        this.expandedClass    = 'sticky-navigation-wrapper--expanded';

        this.init();
    }

    detectScreenHeight(){
        return window.screen.height;
    }
    detectOffsetTop() {
        return window.pageYOffset;
    }
    detectNavigationWrapperHeight() {
        return this.wrapper.clientHeight;
    }
    detectNavigationHeight(){
        return this.navigation.clientHeight;
    }
    detectStickyNavigationHeight(){
        let height = 84;
        if ( this.navigation.classList.contains(this.stickyClass) ) {
            height = this.navigation.clientHeight + 20
        }
        return height;
    }
    detectNavigationWrapperPosition(){
        return this.wrapper.offsetTop;
    }
    detectParentBottomPosition() {
        return this.detectNavigationWrapperPosition() + this.parent.clientHeight - this.detectNavigationHeight(); // 64 - fixed nav height
    }
    detectWebsiteHeaderHeight() {
        return this.websiteHeader.clientHeight;
    }
    detectWebsiteBarHeight() {
        return this.websiteBar.clientHeight;
    }
    toggleNavigation() {
        //get categories stuck if last element at the top of screen
        if ( this.detectOffsetTop() > this.anchor.offsetTop - 84 ) {
            if ( !this.navigation.classList.contains(this.stickyClass) ) {
                this.wrapper.style.height = this.detectNavigationHeight() + 'px';
                this.navigation.classList.add(this.stickyClass);
            }
        }
        //get categories unstuck if last element at the top of screen
        else if ( this.detectOffsetTop() < this.anchor.offsetTop && this.navigation.classList.contains(this.stickyClass) ) {
            this.navigation.classList.remove(this.stickyClass);
            this.wrapper.style.height = '';
        }
        //get categories unstuck
        else {
            this.navigation.classList.remove(this.stickyClass);
            this.wrapper.style.height = '';
        }
    }
    removeHiddenClasses() {
        const array = [...this.elements];
        array.forEach(element => {
            element.classList.remove(this.hiddenClass);
        });
    }

    getNavigationWidth(){
        if ( this.wrapper === null ) return false;
        return this.wrapper.clientWidth
    }
    getElementsWidth(){
        let width = 0;
        [...this.elements].forEach(element => {
            const style = window.getComputedStyle(element, null);
            width += element.offsetWidth + parseInt(style.marginRight);

            if (width > this.getNavigationWidth() ) {
                element.classList.add(this.hiddenClass);
            }
            else {
                element.classList.remove(this.hiddenClass);
            }
        });

        return width;
    }
    getNavigationToggleable(){
        if ( this.getElementsWidth() > this.getNavigationWidth() ) {
            this.wrapper.classList.add( this.initClass );

            this.getHiddenElements();
            this.getMoreButtonReady();
        }
    }
    getHiddenElements(){
        return this.navigation.querySelectorAll(`.${this.hiddenClass}`).length;
    }
    getMoreButtonReady(){
        this.more.innerText = `Eще ${this.getHiddenElements()}`;
        this.more.setAttribute('data-collapsed', `Eще ${this.getHiddenElements()}`)
    }

    addHiddenClasses() {
        const array = [...this.elements];
        array.forEach(element => {
            element.classList.add(this.hiddenClass);
        });
    }
    changeTextHadler(element, text){
        element.innerText = text;
    }
    toggleMore(){
        if ( typeof(this.more) != 'undefined' && this.more != null ) {
            this.more.addEventListener('click', (e)=>{
                const wrapper = e.target.closest('#js-sticky-navigation-wrapper');

                //e.target.style.display = 'none';
                if ( ! wrapper.classList.contains(this.expandedClass) ) {
                    wrapper.classList.add(this.expandedClass);
                    this.changeTextHadler(e.target, e.target.getAttribute('data-expanded'));
                    //this.removeHiddenClasses();
                }
                else {
                    wrapper.classList.remove(this.expandedClass);
                    this.changeTextHadler(e.target, e.target.getAttribute('data-collapsed'));
                    //this.addHiddenClasses();
                }
            });
        }
    }
    enableNavigation() {
        window.addEventListener('scroll', ()=>{
            this.toggleNavigation();
        }, {passive: true});
    }
    init() {
        if (this.parent && window.deviceWidth <= WIDTH._767) {
            this.enableNavigation();
        }

        window.addEventListener('DOMContentLoaded', ()=>{
            this.getNavigationToggleable();
        });
        window.addEventListener('resize', ()=>{
            this.getNavigationToggleable();
        });
        this.toggleMore();
    }
}
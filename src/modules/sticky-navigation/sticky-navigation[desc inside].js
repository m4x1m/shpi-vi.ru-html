/*
* Когда общая высота всех пунктов
* больше высоты экрана телефон, то
* меню фиксириуется, когда последний появляется
* снизу экрана на ~10px выше фиксированной плашки
*/

class StickyNavigation {
    constructor() {
        this.parent           = document.getElementById('js-sticky-navigation-parent');
        this.wrapper          = document.getElementById('js-sticky-navigation-wrapper');
        this.navigation       = document.getElementById('js-sticky-navigation');
        this.elements         = document.querySelectorAll('.js-sticky-navigation-element');
        this.more             = document.getElementById('js-sticky-navigation-more');
        this.anchor           = document.getElementById('js-sticky-navigation-anchor');
        this.websiteHeader    = document.getElementById('js-website-header');
        this.websiteBar       = document.querySelector('.website-bar');
        this.stickyClass      = 'sticky-navigation--sticky';
        this.hiddenClass      = 'sticky-navigation__element--hidden';

        this.init();
    }

    detectScreenHeight(){
        return window.screen.height;
    }
    detectOffsetTop() {
        return window.pageYOffset;
    }
    detectNavigationWrapperHeight() {
        return this.wrapper.clientHeight;
    }
    detectNavigationHeight(){
        return this.navigation.clientHeight;
    }
    detectStickyNavigationHeight(){
        let height;
        if ( this.navigation.classList.contains(this.stickyClass) ) {
            height = this.navigation.clientHeight
        }
        return height;
    }
    detectNavigationWrapperPosition(){
        return this.wrapper.offsetTop;
    }
    detectParentBottomPosition() {
        return this.detectNavigationWrapperPosition() + this.parent.clientHeight - this.detectNavigationHeight(); // 64 - fixed nav height
    }
    detectWebsiteHeaderHeight() {
        return this.websiteHeader.clientHeight;
    }
    detectWebsiteBarHeight() {
        return this.websiteBar.clientHeight;
    }
    toggleNavigation() {
        //categories height > screen height
        if ( this.detectNavigationWrapperHeight() > this.detectScreenHeight() ) {
            if ( this.detectOffsetTop() + this.detectScreenHeight() - this.detectWebsiteBarHeight() - 50 >= this.detectNavigationWrapperPosition() + this.wrapper.clientHeight ) {
                if ( !this.navigation.classList.contains(this.stickyClass) ) {
                    this.navigation.classList.add(this.stickyClass);
                    this.wrapper.style.height = this.detectStickyNavigationHeight() + 'px';

                    window.scrollTo(0, this.anchor.offsetTop - this.detectWebsiteHeaderHeight() - 10);
                }
            }
            else {
                this.navigation.classList.remove(this.stickyClass);
                this.wrapper.style.height = '';
            }
        }
        //categories height < screen height
        else {
            if ( this.detectOffsetTop() >= this.detectNavigationWrapperPosition() && this.detectOffsetTop() <= this.detectParentBottomPosition() ) {
                if ( !this.navigation.classList.contains(this.stickyClass) ) {
                    this.navigation.classList.add(this.stickyClass);
                    this.wrapper.style.height = this.detectStickyNavigationHeight() + 20 + 'px'; //64px + 20px
                }
            }
            else {
                this.navigation.classList.remove(this.stickyClass);
                this.wrapper.style.height = '';
            }
        }
    }
    removeHiddenClasses() {
        const array = [...this.elements];
        array.forEach(element => {
            element.classList.remove(this.hiddenClass);
        });
    }
    toggleMore(){
        if ( typeof(this.more) != 'undefined' && this.more != null ) {
            this.more.addEventListener('click', (e)=>{
                this.removeHiddenClasses();
                e.target.style.display = 'none';
            });
        }
    }
    enableNavigation() {
        window.addEventListener('scroll', ()=>{
            this.toggleNavigation();
        }, {passive: true});
    }
    init() {
        if (this.parent && window.deviceWidth <= WIDTH._767) {
            this.enableNavigation();
        }
        this.toggleMore();
    }
}
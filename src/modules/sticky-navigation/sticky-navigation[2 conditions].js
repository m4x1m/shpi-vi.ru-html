/*
* Когда общая высота всех пунктов
* больше высоты экрана телефон, то
* меню фиксириуется, когда последний
* доходит до верха экрана
*/

class StickyNavigation {
    constructor() {
        this.parent           = document.getElementById('js-sticky-navigation-parent');
        this.wrapper          = document.getElementById('js-sticky-navigation-wrapper');
        this.navigation       = document.getElementById('js-sticky-navigation');
        this.elements         = document.querySelectorAll('.js-sticky-navigation-element');
        this.more             = document.getElementById('js-sticky-navigation-more');
        this.anchor           = document.getElementById('js-sticky-navigation-anchor');
        this.websiteHeader    = document.getElementById('js-website-header');
        this.websiteBar       = document.querySelector('.website-bar');
        this.stickyClass      = 'sticky-navigation--sticky';
        this.hiddenClass      = 'sticky-navigation__element--hidden';

        this.init();
    }

    detectScreenHeight(){
        return window.screen.height;
    }
    detectOffsetTop() {
        return window.pageYOffset;
    }
    detectNavigationWrapperHeight() {
        return this.wrapper.clientHeight;
    }
    detectNavigationHeight(){
        return this.navigation.clientHeight;
    }
    detectStickyNavigationHeight(){
        let height = 84;
        if ( this.navigation.classList.contains(this.stickyClass) ) {
            height = this.navigation.clientHeight + 20
        }
        return height;
    }
    detectNavigationWrapperPosition(){
        return this.wrapper.offsetTop;
    }
    detectParentBottomPosition() {
        return this.detectNavigationWrapperPosition() + this.parent.clientHeight - this.detectNavigationHeight(); // 64 - fixed nav height
    }
    detectWebsiteHeaderHeight() {
        return this.websiteHeader.clientHeight;
    }
    detectWebsiteBarHeight() {
        return this.websiteBar.clientHeight;
    }
    toggleNavigation() {
        //categories height > screen height
        if ( this.detectNavigationWrapperHeight() > this.detectScreenHeight() ) {
            //get categories stuck if last element at the top of screen
            if ( this.detectOffsetTop() > this.anchor.offsetTop - 84 ) {
                if ( !this.navigation.classList.contains(this.stickyClass) ) {
                    this.wrapper.style.height = this.detectNavigationHeight() + 'px';
                    this.navigation.classList.add(this.stickyClass);
                }
            }
            //get categories unstuck if last element at the top of screen
            else if ( this.detectOffsetTop() < this.anchor.offsetTop && this.navigation.classList.contains(this.stickyClass) ) {
                this.navigation.classList.remove(this.stickyClass);
                this.wrapper.style.height = '';
            }
            //get categories unstuck
            else {
                this.navigation.classList.remove(this.stickyClass);
                this.wrapper.style.height = '';
            }
        }
        //categories height < screen height
        else {
            if ( this.detectOffsetTop() >= this.detectNavigationWrapperPosition() && this.detectOffsetTop() <= this.detectParentBottomPosition() ) {
                if ( !this.navigation.classList.contains(this.stickyClass) ) {
                    this.navigation.classList.add(this.stickyClass);
                    this.wrapper.style.height = this.detectStickyNavigationHeight() + 'px';
                }
            }
            else {
                this.navigation.classList.remove(this.stickyClass);
                this.wrapper.style.height = '';
            }
        }
    }
    removeHiddenClasses() {
        const array = [...this.elements];
        array.forEach(element => {
            element.classList.remove(this.hiddenClass);
        });
    }
    toggleMore(){
        if ( typeof(this.more) != 'undefined' && this.more != null ) {
            this.more.addEventListener('click', (e)=>{
                this.removeHiddenClasses();
                e.target.style.display = 'none';
            });
        }
    }
    enableNavigation() {
        window.addEventListener('scroll', ()=>{
            this.toggleNavigation();
        }, {passive: true});
    }
    init() {
        if (this.parent && window.deviceWidth <= WIDTH._767) {
            this.enableNavigation();
        }
        this.toggleMore();
    }
}
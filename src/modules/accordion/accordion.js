const accordion = (container, collapsible) => {
    if ( $(container).length ) {
        const ACC = {
            parent        : '.js-accordion',
            button        : '.js-accordion-button',
            body          : '.js-accordion-body',
            hiddenClass   : '_hide',
            expandedClass : '_expanded'
        };

        $(container).find(ACC.parent).each(function(){
            const button = $(this).find(ACC.button);

            if (!$(this).hasClass(ACC.expandedClass)) {
                $(this).find(ACC.body).removeClass(ACC.hiddenClass).hide();
            }
            else {
                $(this).find(ACC.body).removeClass(ACC.hiddenClass);
            }

            button.on('click', function(){
                const accordion      = $(this).parents(ACC.parent);
                const $container     = accordion.parents(container);
                const $accordionBody = accordion.find(ACC.body);

                if ( !accordion.hasClass(ACC.expandedClass) ) {
                    if (collapsible) {
                        $container.find(ACC.parent).removeClass(ACC.expandedClass);
                        $container.find(ACC.body).stop().slideUp(300);
                    }
                    accordion.addClass(ACC.expandedClass);
                    $accordionBody.stop().slideDown(300);
                }
                else {
                    accordion.removeClass(ACC.expandedClass);
                    accordion.find(ACC.body).stop().slideUp(300);
                }

                //scroll to top of opened accordion on mobile devices
                if ( collapsible && window.deviceWidth <= APP.tabletXsWidth ) {
                    setTimeout(function(){
                        const offset = accordion.offset().top - APP.websiteHeader.height() - 10;
                        $('html, body').animate({ scrollTop: offset }, '200');
                    }, 400);
                }
            });
        });
    }
};
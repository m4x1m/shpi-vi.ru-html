const selectStyler = () => {
    const select = $('.select select');
    if (select.length) {
        select.each(function(){
            const $this = $(this);

            if ( !$this.find('.jq-selectbox').length ) {
                $this.styler({
                    selectSmartPositioning  : true,
                    selectPlaceholder       : '',
                    selectSearch            : true,
                    selectSearchPlaceholder : 'Поиск',
                    selectSearchNotFound    : 'Ничего не найдено',
                    selectSearchLimit       : 10,
                    singleSelectzIndex      : 10,
                    onFormStyled            : function(){
                        selectPlaceholderHandler($('.select'));
                    },
                    onSelectOpened          : function() {}
                });
            }
            else {
                $this.styler('refresh');
            }

            $(document).on('click', '.select', function(){
                const dropdown = $(this).find('ul');
                if ( !dropdown.hasClass('ps') ) {
                    const customScrollBar = new CustomScrollbar(dropdown);
                }
            });
        });
    }
};

const selectPlaceholderHandler = (element) => {
    element.each(function(){
        const label     = $(this).attr('data-select-label');
        const selectbox = $(this).find('.jq-selectbox');
        if ( !selectbox.find('.select__label').length ) {
            $('<div />', {
                class : 'select__label',
                html  : label
            }).prependTo(selectbox);
        }
    });
};
const messengers = () => {
    const PARAMS = {
        button : [...document.querySelectorAll('.js-messengers-button')],
        openedClass : '_messengers-is-opened',

        expandHandler(){
            APP.body.classList.add(PARAMS.openedClass);
        },
        collapseHandler(){
            APP.body.classList.remove(PARAMS.openedClass);
        },
        bindEvent(){
            PARAMS.button.forEach(button => {
                button.addEventListener('click', ()=>{
                    if ( !APP.body.classList.contains(PARAMS.openedClass) ) {
                        PARAMS.expandHandler();
                    }
                    else {
                        PARAMS.collapseHandler();
                    }
                });
            });
        },
        init() {
            PARAMS.bindEvent();
        }
    };
    PARAMS.init();
};
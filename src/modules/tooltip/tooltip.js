const tooltip = (width) => {
    let PARAMS = {
        el          : $('.js-tooltip'), // === helper.origin
        trigger     : 'hover',
        arrowRight  : 'right',
        arrowLeft   : 'left',
        theme       : 'tooltipster-shadow'
    };

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        PARAMS.trigger = 'click';
    }

    $(window).on('load resize', function(){
        if ( window.deviceWidth < width ) {
            PARAMS.arrowRight  = 'bottom';
            PARAMS.arrowLeft   = 'bottom';
            PARAMS.trigger     = 'click';
        }
    });

    if ( PARAMS.el.length ) {

        PARAMS.el.tooltipster({
            trigger      : PARAMS.trigger,
            theme        : PARAMS.theme,
            contentAsHTML: true,
            interactive  : true,
            zIndex       : 146,
            delay        : [100, 300], // open/close,
            distance     : -5,
            functionPosition: function(instance, helper, position){
                if ( typeof $(helper.origin).attr('data-distance') != 'undefined' ) {
                    PARAMS.distance = parseInt( $(helper.origin).attr('data-distance') );
                    position.coord.top += PARAMS.distance;
                    return position;
                }
            },
            functionReady: function(instance, helper){
                if ( typeof $(helper.origin).attr('data-fav-added') != 'undefined' ) {
                    //favouritesHandler(helper, instance, PARAMS.trigger);
                }
            },
            functionAfter: function(instance, helper) {
                if ( typeof $(helper.origin).attr('data-fav-added') != 'undefined' ) {
                    $(helper.origin).off('click');
                }
            }
        });
    }
    const favouritesHandler = (helper, instance, trigger ) => {
        const PARAMS = {
            element     : $(helper.origin),
            activeClass : '_active',
        };
        PARAMS.element.on('click', function(){
            const $this = $(this);
            const $text = $this.find('.js-tooltip-text');

            if ( !$this.hasClass(PARAMS.activeClass) ) {
                $this.addClass(PARAMS.activeClass);

                if ( trigger === 'hover' ) {
                    PARAMS.element.tooltipster('open');
                    instance.content(PARAMS.element.attr('data-fav-added'));
                }
                else {
                    instance.content(PARAMS.element.attr('data-fav-added'));
                    setTimeout(timeoutTrigger.bind(this), 1500);
                }

                if ( typeof $(helper.origin).attr('data-fav-changeable') != 'undefined' ) {
                    $text.text(PARAMS.element.attr('data-fav-added'));
                }
            }
            else {
                $this.removeClass(PARAMS.activeClass);

                if ( trigger === 'hover' ) {
                    PARAMS.element.tooltipster('open');
                    instance.content(PARAMS.element.attr('data-fav-default'));
                }
                else {
                    instance.content(PARAMS.element.attr('data-fav-default'));
                    setTimeout(timeoutTrigger.bind(this), 1500);
                }

                if ( typeof $(helper.origin).attr('data-fav-changeable') != 'undefined' ) {
                    $text.text(PARAMS.element.attr('data-fav-changeable'));
                }
            }

        });

        function timeoutTrigger() {
            PARAMS.element.tooltipster('close');
        }
    }
};
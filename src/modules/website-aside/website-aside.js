class WebsiteAside {
    constructor () {
        this.body           = APP.body;
        this.button         = document.querySelectorAll('.js-website-aside-button');
        this.stickyClass    = '_website-aside-is-opened';
        this.scrollPosition = 0;

        this.init();
    }

    currentScrollPosition() {
        return window.pageYOffset;
    }
    preventDefault(e) {
        e.preventDefault();
    }
    disableScroll(){
        document.body.addEventListener('touchmove', this.preventDefault, { passive: false });
    }
    enableScroll(){
        document.body.removeEventListener('touchmove', this.preventDefault);
    }
    toggleAside() {
        if ( !this.body.classList.contains(this.stickyClass) ) {
            this.scrollPosition = this.currentScrollPosition();
            this.body.classList.add(this.stickyClass);
            //this.disableScroll();
        }
        else {
            this.body.classList.remove(this.stickyClass);
            window.scrollBy(0, this.scrollPosition);
            //this.enableScroll();
        }
    }
    init() {
        [...this.button].forEach(button => {
           button.addEventListener('click', ()=>{
               this.toggleAside();
           });
        });
    }
}
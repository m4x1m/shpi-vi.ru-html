const yandexMapInit = () => {
    const PARAMS = {
        triggerClass : document.querySelectorAll('.js-yandex-map-trigger'),
        activeClass  : 'yandex-map--initialized'
    };
    window.APIAdded = false;

    if ( PARAMS.triggerClass.length > 0 ) {
        const array = [...PARAMS.triggerClass];
        array.forEach(element => {
            element.addEventListener('click',()=>{
                const id = element.getAttribute('data-yamap-id');
                if ( !document.getElementById(id).classList.contains(PARAMS.activeClass) ) {
                    document.getElementById(id).classList.add(PARAMS.activeClass);
                    yandexMap(id);
                }
            });
        });
    }

    if ( document.getElementById('js-courier-map') != null ) {
        if ( !document.getElementById('js-courier-map').classList.contains(PARAMS.activeClass) ) {
            document.getElementById('js-courier-map').classList.add(PARAMS.activeClass);
            yandexMap('js-courier-map');
        }
    }
};
const yandexMap = (id) => {
    const mapID = $('#'+id);

    if ( mapID.length ) {
        const MAP_DATA = {
            coords  : [parseFloat(mapID.attr('data-lat')), parseFloat(mapID.attr('data-lon'))],
            hint    : mapID.attr('data-hint'),
            content : mapID.attr('data-content'),
            icon    : mapID.attr('data-icon'),
            clusterIcon : mapID.attr('data-cluster-icon'),
            iconActive : mapID.attr('data-icon-active'),
            zoom    : parseInt(mapID.attr('data-zoom')),
            file    : mapID.attr('data-file'),
            zoomIn  : mapID.parents('.js-yandex-map').find('.js-ya-maps-zoom-in'),
            zoomOut : mapID.parents('.js-yandex-map').find('.js-ya-maps-zoom-out'),
            full    : mapID.parents('.js-yandex-map').find('.js-ya-maps-full'),
            fullScreenClass : '_in-fullscreen-mode'
        };
        const MAP_FILTER = {
            select      : $('.js-yandex-map-select'),
        };
        let balloonContent = `<div class="yandex-map-route">
                                <div class="yandex-map-route__title">${MAP_DATA.content}</div>
                            </div>`;
        let clusterIcon = 'black';
        //#333333
        let clusterIconImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAMAAADWZboaAAABvFBMVEUzMzP////////b29vW1tbMzMzCwsLAwMC9vb21tbWzs7OxsbGvr6+0tLSzs7Onp6empqaVlZWXl5eRkZGQkJCRkZGPj4+Hh4eDg4OBgYGFhYWGhoaDg4N8fHx6enp7e3t6enp7e3t5eXl5eXlpaWlpaWlnZ2doaGhnZ2dsbGxtbW1kZGRlZWViYmJjY2NlZWVnZ2doaGhdXV1cXFxdXV1QUFBRUVFZWVlVVVVWVlZXV1dJSUlLS0tHR0dISEg9PT05OTk6Ojo7Ozs9PT1AQEBBQUEzMzM0NDQ1NTU2NjY3Nzc4ODg5OTk9PT0+Pj4/Pz9AQEBDQ0NERERFRUVNTU1XV1dZWVlbW1tdXV1eXl5kZGRlZWVvb29wcHBxcXF0dHR1dXV2dnZ3d3d5eXl6enp9fX1+fn5/f3+BgYGCgoKDg4OEhISJiYmKioqMjIyPj4+QkJCTk5OUlJSfn5+goKChoaGtra2urq6vr6+wsLCxsbG1tbXJycnNzc3Ozs7Pz8/f39/g4ODh4eHi4uLk5OTl5eXm5ubn5+fo6Ojp6ent7e3u7u7w8PDx8fHy8vLz8/P19fX6+vr7+/v///97CwCeAAAARnRSTlMAAAEiIzQ2NzhYWWBhYmNkZXh4l5iYmaKmp7Gxs7e4uL29vr/V1tfX2Nra3Nzd3d3d3efo6PDw8PHx8fr6+/v9/v7+/v7+r3RYDAAAAuhJREFUeNpiwAqYhWS1zG0BrJYDlyQxFEYzXtu2bbfOfkmdnrbGtm3byg8eTzrVW5Xumdp7VLoVv/dcgOv3j/d3LmSQOGkCAy/72icHdDg+XctJQT331A6wWFP/6Mwa52szo/1NMQbYn55Loh5/zUALexa5jsWeQgr2+rhCzbhvh7t+ghswUe+G/V6GmXrmC1A5y02YqwQ+nzFWL9oQHuIKhsKwXTZQ028wlK5wJStlYDfSE9X0B6BdPCldFA/SE9SbYIM8BQYZburVy4z28JTooeySrJ62oZOnSCdsp+Nq5heUJS5Ff3U0pGmhaHX/HNdThs+ZQr2P8CqXGS7xuhkFAMrcnuJhLrMaxr0D9YQDuvWcKPEwSDBPybhufeE4sa++QpVuIsIaEtDCukmswss99STLlYfTHoABgXZ5InLZqV31EZp4nA4/DPF38DhNeLyjZjnphNTbIEwISn2epM6sbfUqCrhgLAxTwmNcUIir2+oH9HJBCRSUcEEfPqWRDAddEE9GPFDgia/vAnVkkvOISI1SKKBSsxGcJ3fRLO5nPVDimZXm+DZ5h35xP6BBiTYQ/xTPyXeMivtaBiWsRnw6iq/kD6bFfZRCCY2KT6fxk7iwJu4DSEJAfLqEvwTgAg1JYOLTdbgtqFY6bGGaLCzOOwwccUtY2IgWtr+VQ0c+oe+IR51cQ+FRAsy1bTXHTie5oCcAEwK6sGbP2VbJE10wbfPBEF+rLpg+243DZ2nu/GFDOD23q5I3h08cb0zS1fi/6WrMOF0ZJckij5Qki4ySpDo1BzUtGK3unVWkZgsFwTZXGO1Lzeyj7Mr/Kn5I+kPQ7uRmt1xyCfcWQ3myQq8C7JZU6Aku2xAeVpnDorw0Kmpp3byZOF9HE4pamYx7drgbjEvpRlFKKwv4viW9t9RXSEG3pstFVLchIg/cbZjU21JflRdJoNuAABySGJ0VY0kOAp0VhKcFZTXNbVy8vFxsLPTlBLF3kQB2onR4p1DbpwAAAABJRU5ErkJggg==';
        if ( typeof MAP_DATA.clusterIcon !== 'undefined' ) {
            clusterIcon = MAP_DATA.clusterIcon;

            if ( clusterIcon === 'red' ) {
                //#ed1651
                clusterIconImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAMAAADWZboaAAAB9VBMVEXtFlH////////81uD70Nz6xdP6usv5t8n5tMf4qsD4qL74pbz4pLv4qb/4qL73mrT3mrP2hqX2iKb1gqL1gKD1gaH1f5/0dpn0cZX0cJT0c5f0dZj0cZXzaY/zZ47zaI7zZ47zaI7zZYzzZYzyVH/yVH/yUX3yUn7yUX3yV4HyWYPxTnvxUHzxTHnxTXrxUHzyUX3yUn7xRnXxRXTxRnTxRnXwN2rwOGrwOWvwQXHwPW7wPm/wP3DvMGTvMWXvLWLvLmPuIVnuIlruHVbuHlfuH1juIVnuJVzuJl3tFlHtF1LtGFLtGVPtGlTtG1TtG1XuHVbuIVnuIlruI1vuJFvuJVzuKF/uKV/vKmDvM2fvNGjwP3DwQXHwQnLxRHPxRnXxR3bxTnvxT3vyW4TyW4XyXIbzXYbzYIjzYYnzYorzY4rzZIvzZYzzZo3zZ47zapD0a5D0bJH0bZL0cJT0cJX0cZX0cpb1eJr1eZv1epv1e531f5/1gKD1hKP2haT3ka33kq73k673lK/4obn4orn4o7r4pLv4pbv4pbz4pr34qsD6wtH7xdT7xtX7x9X7yNb82uT82+T83OX83eb83ub94Oj94ej94un94+r95Ov95ev95ez96/D+7PH+7vL+7/P+8PT+8fX+8/b+9Pf/+vv/+vz////k+xRoAAAASXRSTlMAAAEiIzQ2NzhYWWBhYmNkZXh4l5iYmaKmp7Gxs7e4uL29vr/V1tfX2Nra3Nzd3d3d3efo6Ojw8PDw8fHx+vr7+/39/v7+/v7+4tYXgAAAAwZJREFUeNqtlwOb9EgURuuzbdse23qrK2Pbtm3bNupvjqeS9KbT2vMouCfOBdHkwv1X3919Q4AQX69/r+9fIIJzMhre5afOQVAR5Pz0igXq3S+BgCG9cWBicYfzncWJgcZ0AxD45a4Z9dovA2h27xpXsdabTWH4dU1HvfAuEBG1M1yDmdoIBL69YEq97QaULnETLJcBLre11Qf+SB7hOowkw/+Rhnr+OUPBJtdlsxDs+Xlj9fx70E5ulk6K9+eN1Bdgw9wChhleqNVHjPZyi+il7KFSvRWADm4hHfC/JasXXVFo/CoGKtMSJSkxrXJgmaspgstFob5D8hZXMpYfE8EoAFAWEZ07ypVsJePtmXo9CKr3OZ0XxaCAReVNqd4vgq6fqj9RrnoQSRKMkJJUD7EcP07UGyxSeTtt8dAgvk35ICINN4/Vj2jgMu1x0CSuncs04NOReimYziiuNgEmSFBc8ywNvnSoPkEWF0wmwSRJk1yQjSeHqgP6uCAPOuRxQT+cz5ELQXRVbBmPgg5RYyJwlQZdJPeQwgX5FDrQfC5IwT3yBk1ifSkaukTLKaQRr8hfDIj1QQm6SINyKL4RT0yI9WoGXViVCJ2AO/HDglhPo9CFporQeXiTEGyL9XiYIV6EriOMAFwgwQxMhO4iwh41BDu2XrAdj8mOl/MXQ7Z8EkP4Zs+HaMfnb8dPR5zRb+OvTp4i25YE8/RQvRJIZ7mgJx4miO9RprXAK4cq+YxGLtMaC01iW7hMI74e5+E7NHLF2hRO7x6r5Lf1heO3iXI19d9yNalRrrSL5GhOtKJI5mgWSVGaizRKc4IkJaRV9C3plWZyy9/GhuCQx4z2W2b2U/b4/2p+yPkPoF3mzW5lyyXclwYUm2v0SmB4KTd6Mo/8kTymZ44Zt5cyt11Ba1ZMiSs11KipVXLhbSAi6rRb6XrRSus28P3ram+9P5uC/rpuydjAMpqPx4a9jbmJweZMZsnYcMTVZ07Gw4rjs6uWzDlHXLj36pu7T2h4eKiPx5/X97RHpANVDOovWCXG1QAAAABJRU5ErkJggg=='
            }
            else if ( clusterIcon === 'green' ) {
                //#1ab248
                clusterIconImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAMAAADWZboaAAAB71BMVEUaskj////////X8d/R8NrG7NG76Mi458a15sSr47yq4run4bml4ber47yq4ruc3rCb3a+I16CK2KKE1p2C1ZuD1ZyB1Zt40pN00JBy0I520ZF30ZN00JBszYlqzYhrzYlqzYhrzYlozIZozIZXx3lXx3lUxndVxndUxndax3tcyHxRxHRTxXVPxHJQxHNTxXVUxndVxndKwm5IwW1Jwm1Kwm46vWI7vWM8vWNEwGpBv2dCv2hCwGgzulw1u14wuloxulsltlEmtlEhtE4itU4jtU8ltlEot1Mqt1Uaskgbskkcs0kds0oes0setEwftEwhtE4ltlEmtlEntlInt1Mot1MsuFYtuFcuuVg3vF84vGBCwGhEwGpFwGpHwWxKwm5Kwm9RxHRSxXRdyX5eyX9fyX9gyoBjyoJky4Nly4RmzIVozIZpzYdqzYhtzopuzotvz4xy0I5z0I900JB10JB60pV705Z805Z+1JiB1ZuC1ZuG1p6H15+T26mU26qV26qW3Kuj4LWj4Lak4bel4bem4bin4bmo4rmr47zD68/G7NLH7NPI7dPJ7dTb8+Lc8+Pd9OTe9OTf9OXg9efh9efi9ejj9unk9unl9urm9+vr+O/s+fDu+fHw+vPx+vTy+vTz+/b0+/b6/fv7/fv///9WZq2CAAAASXRSTlMAAAEiIzQ2NzhYWWBhYmNkZXh4l5iYmaKmp7Gxs7e4uL29vr/V1tfX2Nra3Nzd3d3d3efo6Ojw8PDw8fHx+vr7+/39/v7+/v7+4tYXgAAAAwhJREFUeNqsloN79Eochaf+bNu22Xb53DM7qW3btm3bnf+z7iTZm83qex/tJr83Ts4hmvhduvvqZ4gVsIb8+Xjvkh+R8RFoeIE3vpqhwvz1RpAL6oXnJsCWVtc7OrfJ+ebcaG9dmg0wPb/gRD351gaa1bXMVSx3ZVHY3p7UUf0emhBeNck1mKwKh+mBnyP13A+geJ47YKEE+HZOW71sQNIg12EwCYarGqrvLYb8Na7LWgHYLV971fcRaBt3ShvFI1879TbYAHeBAYbbavUqo13cJboou6JUzxrRyl2kFYazsur/HQX2t6K3PDVBkhJSy3sXuJpCfPMX6kMkrXMlw3nR4YwCAGXhUTlDXMl6Eh4cq6fMUN3PidxIBgUsMndcdX9hPnWkvkGp6kIkSrBDSlRdxFK8PlRPswjl6TTHQYO4ZuWFiLCdOVCfoJbLtMRCk9gWLlOLp/tqgIVOKo42Hg6IVxzzFLUE7KnXkckFY4lwSOIYF2Th+p76Cd1ckAsdcrmgB199iJ+ZLoklI5HQIXJYDC5Rsz+5iGQuyKPQgeZxQTIukvuoF//no6BLlPwJqcNd8gG94n+fBF2kPnkUL8lvjIr/FQy6sHIxOoqfJBSz4n8qhS40RYzO4C+xYkP8j4MT4sToCv4jABdIcAITo1sI90a1YtPTA/biMnlxcz6g35NHoh8vvXkQvXj8vXjpyFf0ePiqkxvI8uQDc2NPDTLRKS7ojIMD4jqVnzVT0J5KnqGOyzTFQJOYRi5Thxc+++p5GrHo7iecXjhQyTv3g+Odg7ga/39cjWnHlUZIDmVHKUIyWzMkRTQXakRzvCTFp5Z1z+tFMzlr8LAQ7HGN0R7XzB7Krv2r8kN8H4O2Ozc7lJVLuHdsKHRW9IpguyMXPZmrBiQN65nD9vVS5tx30MpFR+JiJbUrtUr8HpgQXq1dpWtEldYt8D0ram+lJ4uC7k6Xi5hug098BbjbsHju5N62igQf/N0GBOCQNEHvrBhLchDorCA8LSiraWnn7ufnbmelLyeIvYsEACYu3xNxEAlqAAAAAElFTkSuQmCC';
            }
            else if ( clusterIcon === 'orange' ) {
                //#f25822
                clusterIconImage = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADoAAAA6CAMAAADWZboaAAAB71BMVEXyWCL////////94tj83tP81cj7zb37y7v7ybj6wq76wa36v6r6vqj6wa76wa35t6D5tp/4qY34qo74pYj4pIf4pYf4o4b3nX33mXn3mHf3m3r3nHz3mXn3lHH3km/3k3D3km/3k3D2kW32kW31hV31hV31g1r1g1v1g1r2hmD2iGH1gFf1gVn1f1X1f1b1gVn1g1r1g1v1e1D1eU71ek/1e1D0cEH0cEL0cUP0d0v0dEf0dUj0dUnzajr0bDzzaDjzaTnzYCzzYS3yXSnyXirzXyvzYCzzYjDzZDLyWCLyWSPyWSTyWiXyWyXyWybyXCfyXSnzYCzzYS3zYS7zYi/zYjDzZTPzZjTzZjX0bT70bj/0dUn0d0v0d0z1eU31e1D1e1H1gFf1gVj2iWP2imT2imX2i2b2jWj2jmn2jmr2j2v2kGz2kW32km73km/3lHL3lXP3lnP3lnT3mHf3mXj3mXn3mnr3nn/4n4D4oYL4o4b4pIf4p4r4p4v5sJf5sZj5spn5spr6vKb6vKf6vaf6vqj6vqn6v6r6v6v6wq7808X81sj81sn818r82Mv95dz95d395t7959/95+D96eL96uP96+T96+X+7Ob+7ef+8ez+8e3+8+/+9PD+9fH+9fL+9vT+9/X/+/r//Pv///9PdZDVAAAASXRSTlMAAAEiIzQ2NzhYWWBhYmNkZXh4l5iYmaKmp7Gxs7e4uL29vr/V1tfX2Nra3Nzd3d3d3efo6Ojw8PDw8fHx+vr7+/39/v7+/v7+4tYXgAAAAwhJREFUeNqsloN79Eochaf+bNu22Xb53DM7qW3btm3bnf+z7iTZm83qex/tJr83Ts4hmvhduvvqZ4gVsIb8+Xjvkh+R8RFoeIE3vpqhwvz1RpAL6oXnJsCW1tA3NrfJ+ebcWF9Dmg0wPb/gRD351gaa1b3MVSx3Z1HY3p7UUf0emhBeM8U1mKoJh+mBnyP13A+gZJ47YKEU+HZOW71sQNIQ12EoCYarGqrvLYb8Na7LWgHYLV971fcRaDt3SjvFI1879TbYIHeBQYbbavUqo93cJbopu6JUzxrRxl2kDYazsur/HQX2t6KvIjVBkhJSK/oWuJpCfPMX6kMkrXMlI3nR4YwCAGXhUTnDXMl6Eh4cq6fMUN3PydxIBgUsMndCdX9hPnWkvkGZ6kIkSrBDSlRdxDK8PlRPswjl6bTEQYO4FuWFiLCdOVCfoJ7LtMZCk9hWLlOPp/tqgIVOKY42Hg6IVxzzNLUE7KnXkckF44lwSOI4F2Th+p76CT1ckAsdcrmgF199iJ+ZLoklo5HQIXJEDC5Rsz+5iGQuyKPQgeZxQTIukvtoEP/no6BLlPwJacBd8gF94n+/BF2kfnkUL8lvjIn/VQy6sEoxOoafJBSz4n8qhS40RYzO4C+xYkP8j4MT4sToCv4jABdIcAITo1sI90a1YtPTA/biMnlxcz5gwJNHYgAvvXkQvXj8vXjpyFf0eviqkxvI8uQDc2NPDTLRaS7oioMD4rqUnzVT0J5KnqGByzTHQJOYJi7TgBc+++p5GrHo7iecXjhQyTv3g+Odg7ia+H9cjWvHlUZIDmdHKUIyWzMkRTQXakRzvCTFp5b3zOtFMzlr8LAQ7HGN0V7XzF7Krv2r8kN8H4N2ODc7lZVLuHdsKHJW9IphuyMXPZmrBiSN6Jkj9vVS5tx30OpFR+JiNbUrtUr8HpgQXqtdpetEldYt8L0ram+lN4uC7k6Xi5hug098JbjbsHjulL72ygQf/N0GBOCQNEHvrBhLchDorCA8LSiraWnn7ufnbmelLyeIvYsEAPtc36ijG7tlAAAAAElFTkSuQmCC';
            }
        }

        loadYandexAPI(()=>{
            if ( typeof MAP_DATA.file !== 'undefined' ) {
                ymaps.ready(function () {
                    const map = new ymaps.Map(id, {
                            center   : MAP_DATA.coords,
                            zoom     : MAP_DATA.zoom,
                            controls : []
                        }),
                        iconContentLayout = ymaps.templateLayoutFactory.createClass(
                            '<div style="color: #ffffff; font-weight: bold;">{{ properties.geoObjects.length }}</div>'
                        ),
                        clusterer = new ymaps.Clusterer({
                            clusterIcons : [
                                {
                                    href    : clusterIconImage,
                                    size    : [58, 58],
                                    offset  : [-24, -24],
                                },
                            ],
                            clusterIconContentLayout : iconContentLayout,
                            groupByCoordinates: false,
                            clusterDisableClickZoom: false,
                            clusterHideIconOnBalloonOpen: false,
                            geoObjectHideIconOnBalloonOpen: true
                        }),
                        balloonLayout = ymaps.templateLayoutFactory.createClass(
                            '<div class="yandex-map-balloon js-yandex-map-balloon">' +
                                '<button type="button" class="yandex-map-balloon__close js-yandex-map-balloon-close"><svg class="i" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M10.8775 1L5.93874 5.93872M5.93874 5.93872L1.28101 10.5964M5.93874 5.93872L1 1M5.93874 5.93872L10.5965 10.5964" stroke-width="2" stroke-linecap="round"/></svg></button>' +
                                '<div class="yandex-map-balloon__arrow js-yandex-map-balloon-arrow"></div>' +
                                '<div class="yandex-map-balloon__inside">' +
                                    '$[[options.contentLayout observeSize]]' +
                                '</div>' +
                            '</div>', {
                                build: function () {
                                    this.constructor.superclass.build.call(this);
                                    this._$element = $('.js-yandex-map-balloon', this.getParentElement());
                                    this.applyElementOffset();
                                    this._$element.find('.js-yandex-map-balloon-close')
                                        .on('click', $.proxy(this.onCloseClick, this));
                                },
                                clear: function () {
                                    this._$element.find('.js-yandex-map-balloon-close')
                                        .off('click');

                                    this.constructor.superclass.clear.call(this);
                                },
                                onSublayoutSizeChange: function () {
                                    balloonLayout.superclass.onSublayoutSizeChange.apply(this, arguments);

                                    if(!this._isElement(this._$element)) {
                                        return;
                                    }

                                    this.applyElementOffset();

                                    this.events.fire('shapechange');
                                },
                                applyElementOffset: function () {

                                    this._$element.css({
                                        top: -(this._$element[0].offsetHeight/2 + 19)
                                    });
                                },
                                onCloseClick: function (e) {
                                    e.preventDefault();

                                    this.events.fire('userclose');
                                },
                                getShape: function () {
                                    if(!this._isElement(this._$element)) {
                                        return balloonLayout.superclass.getShape.call(this);
                                    }

                                    const position = this._$element.position();

                                    return new ymaps.shape.Rectangle(new ymaps.geometry.pixel.Rectangle([
                                        [position.left, position.top], [
                                            position.left + this._$element[0].offsetWidth,
                                            position.top + this._$element[0].offsetHeight + this._$element.find('.js-yandex-map-balloon-arrow')[0].offsetHeight
                                        ]
                                    ]));
                                },
                                _isElement: function (element) {
                                    return element && element[0] && element.find('.js-yandex-map-balloon-arrow')[0];
                                }
                            }),

                        // Создание вложенного макета содержимого балуна.
                        balloonLayoutContent = ymaps.templateLayoutFactory.createClass(
                            '<h5 class="yandex-map-balloon__headline">$[properties.heading]</h5>' +
                            '<div class="yandex-map-balloon__description">' +
                                '<p>$[properties.desc]</p>' +
                            '</div>'
                        );


                    //let collection = {};
                    let geoObjects = [];

                    if ( typeof window.yandexMapPoints !== 'undefined' ) {
                        $.each(window.yandexMapPoints, function(i, element){
                            let elementIcon = MAP_DATA.icon;
                            if ( typeof element.icon !== 'undefined' ) {
                                elementIcon = element.icon;
                            }

                            const placemark = new ymaps.Placemark(element.coordinates, {
                                heading : element.heading,
                                desc    : element.desc,
                                phone   : element.phone
                            },{
                                balloonShadow: false,
                                balloonLayout: balloonLayout,
                                balloonContentLayout: balloonLayoutContent,
                                balloonPanelMaxMapArea: 0,
                                hideIconOnBalloonOpen: false,
                                iconLayout: 'default#image',
                                iconImageHref: elementIcon,
                                iconImageSize: [20, 30],
                                iconImageOffset: [-10, -30]
                            });

                            geoObjects[i] = placemark;
                        });

                        clusterer.add(geoObjects);
                        map.geoObjects.add(clusterer);

                        //change location
                        $(document).on('change', '.select select', function () {
                            var val = $(this).val();
                            if (!val || val == 0) return false;
                            var option = $(this).find('[value="' + val + '"]');
                            var lat = parseFloat(option.attr('data-lat'));
                            var lon = parseFloat(option.attr('data-lon'));

                            if (isNaN(lat) || isNaN(lon)) {
                                $.getJSON('/ajax/json/getCityCoords.php', {'city': option.text()}, function (coords) {
                                    if (Object.keys(coords).length === 2) {
                                        yandexMapLocationHandler(map, [parseFloat(coords.lat), parseFloat(coords.lon)]);
                                    }
                                });
                            }
                            else {
                                yandexMapLocationHandler(map, [lat, lon]);
                            }
                        });
                    }
                    else {
                        $.ajax({
                            url: MAP_DATA.file
                        }).done(function(data) {
                            $.each(data['elements'], function(i, element){
                                let elementIcon = MAP_DATA.icon;
                                if ( typeof element.icon !== 'undefined' ) {
                                    elementIcon = element.icon;
                                }

                                const placemark = new ymaps.Placemark(element.coordinates, {
                                    heading : element.heading,
                                    desc    : element.desc,
                                    phone   : element.phone
                                },{
                                    balloonShadow: false,
                                    balloonLayout: balloonLayout,
                                    balloonContentLayout: balloonLayoutContent,
                                    balloonPanelMaxMapArea: 0,
                                    hideIconOnBalloonOpen: false,
                                    iconLayout: 'default#image',
                                    iconImageHref: elementIcon,
                                    iconImageSize: [20, 30],
                                    iconImageOffset: [-10, -30]
                                });

                                geoObjects[i] = placemark;
                            });

                            clusterer.add(geoObjects);
                            map.geoObjects.add(clusterer);

                            //change location
                            $(document).on('change', '.select select', function () {
                                var val = $(this).val();
                                if (!val || val == 0) return false;
                                var option = $(this).find('[value="' + val + '"]');
                                var lat = parseFloat(option.attr('data-lat'));
                                var lon = parseFloat(option.attr('data-lon'));

                                if (isNaN(lat) || isNaN(lon)) {
                                    $.getJSON('/ajax/json/getCityCoords.php', {'city': option.text()}, function (coords) {
                                        if (Object.keys(coords).length === 2) {
                                            yandexMapLocationHandler(map, [parseFloat(coords.lat), parseFloat(coords.lon)]);
                                        }
                                    });
                                }
                                else {
                                    yandexMapLocationHandler(map, [lat, lon]);
                                }
                            });
                        });
                    }

                    /*map.geoObjects.events
                        .add('balloonopen', function (e) {
                            e.get('target').options.set('iconImageHref', MAP_DATA.iconActive);
                        })
                        .add('balloonclose', function (e) {
                            e.get('target').options.set('iconImageHref', MAP_DATA.icon);
                        });*/

                    const zoomControls = ymaps.templateLayoutFactory.createClass(
                        "", {
                            build: function () {
                                zoomControls.superclass.build.call(this);
                                this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                                this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);
                                MAP_DATA.zoomIn.bind('click', this.zoomInCallback);
                                MAP_DATA.zoomOut.bind('click', this.zoomOutCallback);
                            },
                            clear: function () {
                                MAP_DATA.zoomIn.unbind('click', this.zoomInCallback);
                                MAP_DATA.zoomOut.unbind('click', this.zoomOutCallback);
                                zoomControls.superclass.clear.call(this);
                            },
                            zoomIn: function () {
                                const map = this.getData().control.getMap();
                                map.setZoom(map.getZoom() + 1, {checkZoomRange: true});
                            },
                            zoomOut: function () {
                                const map = this.getData().control.getMap();
                                map.setZoom(map.getZoom() - 1, {checkZoomRange: true});
                            },
                        }),
                        zoomControl = new ymaps.control.ZoomControl({options: {layout: zoomControls}});

                    const fullScreen = ymaps.templateLayoutFactory.createClass(
                        '<button class="yandex-map__button js-ya-maps-full yandex-map__button--fullscreen" type="button">' +
                            '<svg class="i _fullscreen-in" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M14 3C14 2.44772 14.4477 2 15 2H21C21.5523 2 22 2.44772 22 3V9C22 9.55228 21.5523 10 21 10C20.4477 10 20 9.55228 20 9V4H15C14.4477 4 14 3.55228 14 3Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M3 14C3.55228 14 4 14.4477 4 15V20H9C9.55228 20 10 20.4477 10 21C10 21.5523 9.55228 22 9 22H3C2.44772 22 2 21.5523 2 21V15C2 14.4477 2.44772 14 3 14Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M21.7071 2.29289C22.0976 2.68342 22.0976 3.31658 21.7071 3.70711L14.7071 10.7071C14.3166 11.0976 13.6834 11.0976 13.2929 10.7071C12.9024 10.3166 12.9024 9.68342 13.2929 9.29289L20.2929 2.29289C20.6834 1.90237 21.3166 1.90237 21.7071 2.29289Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M10.7071 13.2929C11.0976 13.6834 11.0976 14.3166 10.7071 14.7071L3.70711 21.7071C3.31658 22.0976 2.68342 22.0976 2.29289 21.7071C1.90237 21.3166 1.90237 20.6834 2.29289 20.2929L9.29289 13.2929C9.68342 12.9024 10.3166 12.9024 10.7071 13.2929Z"/></svg>' +
                            '<svg class="i _fullscreen-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M14 3C14 2.44772 14.4477 2 15 2H21C21.5523 2 22 2.44772 22 3V9C22 9.55228 21.5523 10 21 10C20.4477 10 20 9.55228 20 9V4H15C14.4477 4 14 3.55228 14 3Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M3 14C3.55228 14 4 14.4477 4 15V20H9C9.55228 20 10 20.4477 10 21C10 21.5523 9.55228 22 9 22H3C2.44772 22 2 21.5523 2 21V15C2 14.4477 2.44772 14 3 14Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M21.7071 2.29289C22.0976 2.68342 22.0976 3.31658 21.7071 3.70711L14.7071 10.7071C14.3166 11.0976 13.6834 11.0976 13.2929 10.7071C12.9024 10.3166 12.9024 9.68342 13.2929 9.29289L20.2929 2.29289C20.6834 1.90237 21.3166 1.90237 21.7071 2.29289Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M10.7071 13.2929C11.0976 13.6834 11.0976 14.3166 10.7071 14.7071L3.70711 21.7071C3.31658 22.0976 2.68342 22.0976 2.29289 21.7071C1.90237 21.3166 1.90237 20.6834 2.29289 20.2929L9.29289 13.2929C9.68342 12.9024 10.3166 12.9024 10.7071 13.2929Z"/></svg>' +
                        '</button>', {}),
                        fullScreenControl = new ymaps.control.FullscreenControl({options: {layout: fullScreen}});


                    fullScreenControl.events.add(["fullscreenenter", "fullscreenexit"], function(e) {
                        const event = e.originalEvent.type;
                        if (event === 'fullscreenenter') {
                            $('.js-ya-maps-full').addClass('_in-fullscreen-mode');
                        }
                        else if (event === 'fullscreenexit') {
                            $('.js-ya-maps-full').removeClass('_in-fullscreen-mode');
                        }
                    });

                    map.controls.add(zoomControl);
                    map.events.add('click', function() {
                        map.balloon.close();
                    });

                    window['changeMapPos'] = function (lat, lng, zoom) {
                        map.setCenter([lat, lng], zoom);
                    };
                    window['closeBallon'] = function () {
                        map.balloon.close();
                    };

                    //map.controls.add(fullScreenControl);
                    //map.behaviors.disable('scrollZoom');

                    /*if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                        map.behaviors.disable('drag');
                    }*/
                });
            }
            else {
                ymaps.ready(function () {
                    const map = new ymaps.Map(id, {
                            center   : MAP_DATA.coords,
                            zoom     : MAP_DATA.zoom,
                            controls : []
                        });

                    const zoomControls = ymaps.templateLayoutFactory.createClass(
                        "", {
                            build: function () {
                                zoomControls.superclass.build.call(this);
                                this.zoomInCallback = ymaps.util.bind(this.zoomIn, this);
                                this.zoomOutCallback = ymaps.util.bind(this.zoomOut, this);
                                MAP_DATA.zoomIn.bind('click', this.zoomInCallback);
                                MAP_DATA.zoomOut.bind('click', this.zoomOutCallback);
                            },
                            clear: function () {
                                MAP_DATA.zoomIn.unbind('click', this.zoomInCallback);
                                MAP_DATA.zoomOut.unbind('click', this.zoomOutCallback);
                                zoomControls.superclass.clear.call(this);
                            },
                            zoomIn: function () {
                                const map = this.getData().control.getMap();
                                map.setZoom(map.getZoom() + 1, {checkZoomRange: true});
                            },
                            zoomOut: function () {
                                const map = this.getData().control.getMap();
                                map.setZoom(map.getZoom() - 1, {checkZoomRange: true});
                            }
                        }),
                        zoomControl = new ymaps.control.ZoomControl({options: {layout: zoomControls}});

                    const fullScreen = ymaps.templateLayoutFactory.createClass(
                        '<button class="yandex-map__button js-ya-maps-full yandex-map__button--fullscreen" type="button">' +
                        '<svg class="i _fullscreen-in" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M14 3C14 2.44772 14.4477 2 15 2H21C21.5523 2 22 2.44772 22 3V9C22 9.55228 21.5523 10 21 10C20.4477 10 20 9.55228 20 9V4H15C14.4477 4 14 3.55228 14 3Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M3 14C3.55228 14 4 14.4477 4 15V20H9C9.55228 20 10 20.4477 10 21C10 21.5523 9.55228 22 9 22H3C2.44772 22 2 21.5523 2 21V15C2 14.4477 2.44772 14 3 14Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M21.7071 2.29289C22.0976 2.68342 22.0976 3.31658 21.7071 3.70711L14.7071 10.7071C14.3166 11.0976 13.6834 11.0976 13.2929 10.7071C12.9024 10.3166 12.9024 9.68342 13.2929 9.29289L20.2929 2.29289C20.6834 1.90237 21.3166 1.90237 21.7071 2.29289Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M10.7071 13.2929C11.0976 13.6834 11.0976 14.3166 10.7071 14.7071L3.70711 21.7071C3.31658 22.0976 2.68342 22.0976 2.29289 21.7071C1.90237 21.3166 1.90237 20.6834 2.29289 20.2929L9.29289 13.2929C9.68342 12.9024 10.3166 12.9024 10.7071 13.2929Z"/></svg>' +
                        '<svg class="i _fullscreen-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M14 3C14 2.44772 14.4477 2 15 2H21C21.5523 2 22 2.44772 22 3V9C22 9.55228 21.5523 10 21 10C20.4477 10 20 9.55228 20 9V4H15C14.4477 4 14 3.55228 14 3Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M3 14C3.55228 14 4 14.4477 4 15V20H9C9.55228 20 10 20.4477 10 21C10 21.5523 9.55228 22 9 22H3C2.44772 22 2 21.5523 2 21V15C2 14.4477 2.44772 14 3 14Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M21.7071 2.29289C22.0976 2.68342 22.0976 3.31658 21.7071 3.70711L14.7071 10.7071C14.3166 11.0976 13.6834 11.0976 13.2929 10.7071C12.9024 10.3166 12.9024 9.68342 13.2929 9.29289L20.2929 2.29289C20.6834 1.90237 21.3166 1.90237 21.7071 2.29289Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M10.7071 13.2929C11.0976 13.6834 11.0976 14.3166 10.7071 14.7071L3.70711 21.7071C3.31658 22.0976 2.68342 22.0976 2.29289 21.7071C1.90237 21.3166 1.90237 20.6834 2.29289 20.2929L9.29289 13.2929C9.68342 12.9024 10.3166 12.9024 10.7071 13.2929Z"/></svg>' +
                        '</button>', {}),
                        fullScreenControl = new ymaps.control.FullscreenControl({options: {layout: fullScreen}});


                    fullScreenControl.events.add(["fullscreenenter", "fullscreenexit"], function(e) {
                        const event = e.originalEvent.type;
                        if (event === 'fullscreenenter') {
                            $('.js-ya-maps-full').addClass('_in-fullscreen-mode');
                        }
                        else if (event === 'fullscreenexit') {
                            $('.js-ya-maps-full').removeClass('_in-fullscreen-mode');
                        }
                    });

                    if ( typeof mapID.attr('data-route') !== 'undefined' ) {
                        //example
                        //https://yandex.ru/dev/maps/jsbox/2.1/router_editor/
                        const type = mapID.attr('data-route');
                        if ( window.yamapsRoute[type] ) {
                            window.yamapsRoute[type].map(el => {
                                ymaps.route(
                                    el.coords,
                                {
                                    mapStateAutoApply: true
                                }).then(function (route) {
                                    map.geoObjects.add(route);
                                },
                                    function (error) {
                                    console.log("Возникла ошибка: " + error.message);
                                });
                            });
                        }
                    }
                    if ( typeof mapID.attr('data-delivery-range') !== 'undefined' ) {
                        //example
                        //https://yandex.ru/dev/maps/jsbox/2.1/polygon/
                        const delivery = mapID.attr('data-delivery-range').split(',');
                        delivery.forEach(element => {
                            if ( window.yamapsDeliveryRange[element] ) {
                               window.yamapsDeliveryRange[element].map(el => {
                                   const deliveryRange = new ymaps.GeoObject({
                                       geometry: {
                                           type        : 'Polygon',
                                           coordinates : el.coords,
                                           fillRule    : 'nonZero'
                                       },
                                   }, {
                                       fillColor   : el.fillColor,
                                       strokeColor : el.strokeColor,
                                       opacity     : el.opacity,
                                       strokeWidth : el.strokeWidth,
                                       strokeStyle : el.strokeStyle
                                   });

                                   map.geoObjects.add(deliveryRange);
                               });
                           }
                        });
                    }

                    map.controls.add(zoomControl);
                    map.events.add('click', function() {
                        map.balloon.close();
                    });
                    //map.behaviors.disable('scrollZoom');

                    window['changeMapPos'] = function (lat, lng, zoom) {
                        map.setCenter([lat, lng], zoom);
                    }
                    window['closeBallon'] = function () {
                        map.balloon.close();
                    };
                });
            }
        });
    }
};
const loadYandexAPI = (callback) => {
    if ( !window.APIAdded ) {
        window.APIAdded = true;

        const script = document.createElement("script");

        //IE
        if (script.readyState){
            script.onreadystatechange = function(){
                if (script.readyState == "loaded" ||
                    script.readyState == "complete"){
                    script.onreadystatechange = null;
                    callback();
                }
            };
        }
        // other browsers
        else {
            script.onload = function(){
                callback();
            };
        }

        script.id    = 'js-yandex-maps-api';
        script.type  = 'text/javascript';
        script.async = true;
        script.src   = '//api-maps.yandex.ru/2.1/?lang=ru_RU&amp;loadByRequire=1?apikey=d3e505a0-357e-4763-be18-fc7eb56dbced';

        const createdScript = document.getElementsByTagName('script')[0];
        createdScript.parentNode.insertBefore(script, createdScript);
    }
    else {
        callback();
    }
};
const yandexMapLocationHandler = (map, coords) => {
    map.panTo(coords, {
        flying : 0, //no zoom in and out while changing location
        safe   : 1
    });
};
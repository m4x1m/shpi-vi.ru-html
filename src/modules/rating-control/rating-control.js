class RatingControl {
    constructor() {
        this.rating        = document.getElementsByClassName('js-rating-control');

        if (this.rating === null)
            return false;

        this.elementClass  = 'js-rating-control-element';
        this.inputClass    = 'js-rating-control-input';
        this.selectedClass = 'rating-control__element--selected';

        this.init();
    }

    addClass(element) {
        element.classList.add(this.selectedClass);
    }
    removeClass(element) {
        element.classList.remove(this.selectedClass);
    }
    bindEvents () {
        const array = [...this.rating];
        array.forEach(rating => {
            const elements = [...rating.getElementsByClassName(this.elementClass)];
            const input    = rating.querySelector(`.${this.inputClass}`);

            elements.forEach(element => {
                const parent = findAncestor(element, 'js-rating-control');
                //select stars
                element.addEventListener('click', ()=>{
                    const onStar = parseInt(element.getAttribute('data-rating'));
                    const stars  = parent.getElementsByClassName(this.elementClass);

                    for (let i = 0; i < stars.length; i++) {
                        this.removeClass(stars[i]);
                    }
                    for (let i = 0; i < onStar; i++) {
                        this.addClass(stars[i]);
                    }

                    //set input value
                    input.value = parent.getElementsByClassName(this.selectedClass).length;
                });
            });

        });
    }
    init() {
        this.bindEvents();
    }
}
const defaultSlider = () => {
    const PARAMS = {
        sliderClass : '.js-default-slider',
        sliderNODE  : document.querySelector('.js-default-slider'),
        slider      : document.getElementsByClassName('js-default-slider')
    };

    if ( PARAMS.sliderNODE ) {
        let autoplay = false;
        let speed    = 5000;
        let delay    = 1000;
        let slides   = PARAMS.sliderNODE.getElementsByClassName('default-slider__element').length;

        if ( typeof PARAMS.sliderNODE.getAttribute('data-delay') != 'undefined' ) {
            autoplay = true;
            delay = parseInt(PARAMS.sliderNODE.getAttribute('data-delay')) * 1000;
        }
        if ( typeof PARAMS.sliderNODE.getAttribute('data-speed') != 'undefined' ) {
            speed = parseInt(PARAMS.sliderNODE.getAttribute('data-speed')) * 1000;
        }

        $(PARAMS.sliderClass).slick({
            autoplay        : autoplay,
            autoplaySpeed   : delay,
            speed           : speed,
            slidesToShow    : 1,
            arrows          : true,
            prevArrow       : '<button type="button" class="slick-arrow slick-prev"><div class="slick-marker"><svg class="i" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.494 31.494" xml:space="preserve"><path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/></svg></div></button>',
            nextArrow       : '<button type="button" class="slick-arrow slick-next"><div class="slick-marker"><svg class="i" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 31.49" xml:space="preserve"><path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/></svg></div></button>',
            dots            : true,
            infinite        : true,
            fade            : true,
            cssEase         : 'linear',
            touchThreshold  : 15,
            responsive      : [
                {
                    breakpoint: 767,
                    settings: {
                        arrows : false
                    }
                }
            ]
        });
    }
};



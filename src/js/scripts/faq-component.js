class FAQComponent {
    constructor(){
        this.component = [...document.querySelectorAll('.js-faq-component')];
        this.questionClass        = '.js-faq-question';
        this.questionButtonClass  = '.js-faq-question-control';
        this.questionNameClass    = '.js-faq-question-name';
        this.answerClass          = '.js-faq-answer';
        this.answerButtonClass    = '.js-faq-answer-control';
        this.answerNameClass      = '.js-faq-answer-name';

        this.inputClass           = '.js-input';
        this.inputFieldClass      = '.js-input-field';
        this.inputFocuesClass     = 'input--focused';

        this.expandedClass = 'faq-component--expanded';

        this.Init();
    }

    ExpandHandler(element){
        element.classList.add(this.expandedClass);
    }
    CollapseHandler(element){
        element.classList.remove(this.expandedClass);
    }
    CollapseAllHandler(){
        if ( document.querySelector(`.js-faq-component.${this.expandedClass}`) !== null ) {
            this.CollapseHandler(document.querySelector(`.js-faq-component.${this.expandedClass}`));
        }
    }
    SetUserNameHandler(input, name){
        const field =  input.querySelector(this.inputFieldClass);

        input.classList.add(this.inputFocuesClass);
        field.value = name + ', ';
        setTimeout(()=>{
            field.focus();
            //place cursor to the end of text
            field.selectionStart = field.selectionEnd = 10000;
        }, 200);
        //focusAndOpenKeyboard(input.querySelector(this.inputFieldClass), 150);
    }
    BindEvent(){
        this.component.forEach(component =>{
            let name;
            const input    = component.querySelector(this.inputClass);
            const question = component.querySelector(this.questionClass);

            if ( question !== null ) {
                const questionButton = question.querySelector(this.questionButtonClass);
                const questionName   = question.querySelector(this.questionNameClass).innerText;

                questionButton.addEventListener('click', ()=>{
                    name = questionName;
                    if ( !component.classList.contains( this.expandedClass ) ) {
                        this.CollapseAllHandler();
                        this.ExpandHandler(component);
                        this.SetUserNameHandler(input, name);
                    }
                    else {
                        this.SetUserNameHandler(input, name);
                    }
                });
            }

            const answers  = [...component.querySelectorAll(this.answerClass)];
            answers.forEach(answer => {
                const answerButton  = answer.querySelector(this.answerButtonClass);
                const answerName    = answer.querySelector(this.answerNameClass).innerText;

                answerButton.addEventListener('click', ()=>{
                    name = answerName;
                    if ( !component.classList.contains( this.expandedClass ) ) {
                        this.CollapseAllHandler();
                        this.ExpandHandler(component);
                        this.SetUserNameHandler(input, name);
                    }
                    else {
                        this.SetUserNameHandler(input, name);
                    }
                });
            });
        });
    }
    Init(){
        this.BindEvent();
    }
}
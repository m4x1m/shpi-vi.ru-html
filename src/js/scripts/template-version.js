const templateVersionHandler = () => {

    const PARAMS = {
        viewport        : document.querySelector('.js-meta-viewport'),
        trigger         : document.querySelector('.js-template-version-trigger'),
        desktopViewport : 'width=1440, user-scalable=yes, viewport-fit=cover',
        mobileViewport  : 'width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no',
    };
    let desktopVersion = false;

    PARAMS.trigger.addEventListener('click', (e)=>{
        e.preventDefault();
        const mobileText = e.target.getAttribute('data-mobile-text');
        const desktopText = e.target.getAttribute('data-desktop-text');

        if ( desktopVersion ) {
            desktopVersion = false;
            e.target.text = desktopText;
            PARAMS.viewport.setAttribute('content', PARAMS.mobileViewport);
        }
        else {
            desktopVersion = true;
            e.target.text = mobileText;
            PARAMS.viewport.setAttribute('content', PARAMS.desktopViewport);
        }
    });
};
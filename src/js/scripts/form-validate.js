const formValidate = () => {
    const PARAMS = {
        form         : $('.js-form'),
        parent       : '.js-input',
        successClass : 'input--success',
        errorClass   : 'input--error',
    };

    if ( PARAMS.form.length ) {
        $.validator.setDefaults({
            debug   : true,
            success : 'valid'
        });
        $.validator.addMethod("validateEmail", function(value, element) {
            if (/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test(value)) {
                return true;
            } else {
                return false;
            }
        });
        $.validator.addMethod("validatePhone", function (value, element) {
            return value.replace(/\D+/g, '').length > 10;
        });
        $.validator.addMethod("reviewMinLength", function (value, element) {
            return value.length >= 50;
        });
        $.validator.addMethod("validateCyrillic", function (value, element) {
            if (/^[аАбБвВгГдДеЕёЁжЖзЗиИйЙкКлЛмМнНоОпПрРсСтТуУфФхХцЦчЧшШщЩъЪыЫьЬэЭюЮяЯ]+$/.test(value)) {
                return true
            }
        });
        $.validator.addMethod("validateName", function (value, element) {
            if (/^([а-яё]+|[a-z]+)$/i.test(value)) {
                return true
            }
        });

        PARAMS.form.each(function(){
            $(this).validate({
                errorClass   : PARAMS.errorClass,
                validClass   : PARAMS.successClass,
                errorElement : "span",
                rules : {
                    NAME : {
                        required : false,
                    },
                    PHONE : {
                        required      : true,
                        validatePhone : true
                    },
                    EMAIL : {
                        required      : false,
                        validateEmail : false
                    },
                    SUBSCRIPTION_EMAIL : {
                        required      : true,
                        validateEmail : true
                    },
                    PASSWORD : {
                        required : true
                    },
                    "DATA[][REVIEW]" : {
                        required        : true,
                        reviewMinLength : true,
                        minlength       : 1
                    },
                    ANON_REVIEW : {
                        required        : true,
                        reviewMinLength : true,
                        minlength       : 1
                    },
                    USER_CITY : {
                        required        : false,
                    },
                    USER_ADDRESS : {
                        required        : false,
                    },
                    ANON_RATING : {
                        required        : false,
                    }
                },
                messages : {
                    NAME               : 'Введите имя',
                    PHONE              : 'Проверьте телефон',
                    EMAIL              : 'Проверьте электронную почту',
                    SUBSCRIPTION_EMAIL : 'Проверьте электронную почту',
                    PASSWORD           : 'Проверьте пароль',
                    "DATA[][REVIEW]"   : 'Отзыв короче 50 символов. Пожалуйста, поправьте',
                    ANON_REVIEW        : 'Отзыв короче 50 символов. Пожалуйста, поправьте',
                    USER_CITY          : 'Обязательное поле',
                    USER_ADDRESS       : 'Обязательное поле',
                    ANON_RATING        : 'Укажите оценку'
                },

                errorPlacement: function(error, element) {
                    error.appendTo(element.parents(PARAMS.parent));
                },
                highlight: function(element, errorClass, validClass) {
                    $(element).parents(PARAMS.parent).addClass(errorClass).removeClass(validClass);
                },
                unhighlight: function(element, errorClass, validClass) {
                    $(element).parents(PARAMS.parent).removeClass(errorClass).addClass(validClass);
                },
                onfocusout: function(element) {
                    if (!this.checkable(element) && (element.name in this.submitted || !this.optional(element))) {
                        this.element(element);
                    }
                },
                checkForm: function() {
                    this.prepareForm();
                    for (let i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++) {
                        if (this.findByName(elements[i].name).length != undefined && this.findByName(elements[i].name).length > 1) {
                            for (let cnt = 0; cnt < this.findByName(elements[i].name).length; cnt++) {
                                this.check(this.findByName(elements[i].name)[cnt]);
                            }
                        } else {
                            this.check(elements[i]);
                        }
                    }
                    return this.valid();
                },
                submitHandler: function(form) {
                    const App       = $(form);
                    const action    = App.attr('action');

                    $.ajax({
                        url         : action,
                        type        : 'POST',
                        processData : false,
                        contentType : false,
                        success : function(){},
                        error   : function(){}
                    });
                }
            });
        });
    }
};
const APP = {
	name			: 'by design-lab',
	html			: document.querySelector('html'),
	body			: document.querySelector('body'),
	websiteWrapper	: document.querySelector('.js-website-wrapper'),
	websiteHeader	: document.getElementById('js-website-header'),
	websiteFooter	: document.querySelector('.js-website-footer'),
	modalTrigger 	: $('.js-modal-trigger'),
	hiddenCarousel  : 'carousel--hidden'
};
const WIDTH = {
	_1199 : 1199,
	_991  : 991,
	_767  : 767,
	_599  : 599,
	_359  : 359,
};
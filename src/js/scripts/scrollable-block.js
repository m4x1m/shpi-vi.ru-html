class ScrollableBlock {
    constructor() {
        this.block = document.querySelectorAll('.js-scrollable-block');
        this.init();
    }
    init() {
        if ( this.block.length ) {
            this.block.forEach(element => {
                if ( !element.classList.contains('ps') ) {
                    const scrollbars = new PerfectScrollbar(element);
                }
            });
        }
    }
}
const swiperUpdateHandler = (id) => {
    const sliders = id.split(',');

    sliders.forEach(element => {
        document.querySelector(element).swiper.update();
    });
};
const websiteOptionsNav = () => {
    const PARAMS = {
        options       : '.js-website-options',
        button        : document.querySelectorAll('.js-website-options-button'),
        expandedClass : '_website-options-expanded',

        handler() {
            if  ( !APP.body.classList.contains(PARAMS.expandedClass) ) {
                APP.body.classList.add(PARAMS.expandedClass);
                allowClickOutside = true;
            }
            else {
                APP.body.classList.remove(PARAMS.expandedClass);
                allowClickOutside = false;
            }

            if ( APP.body.classList.contains('_website-aside-is-opened') ) {
                APP.body.classList.remove('_website-aside-is-opened');
            }
        }
    };
    let allowClickOutside = false;

    PARAMS.button.forEach(element => {
        element.addEventListener('click', (e)=>{
            if ( window.deviceWidth <= WIDTH._991 ) {
                e.preventDefault();
                PARAMS.handler();
            }
        });
    });

    //hide dropdown on click outside
    document.addEventListener('click', (e)=>{
        let isClickInside = document.querySelector(PARAMS.options).contains(e.target);
        if (!isClickInside && allowClickOutside) {
            APP.body.classList.remove(PARAMS.expandedClass);
        }
    });
};
const photoGallery = (id) => {
    if ( id.length ) {
        id.magnificPopup({
            delegate            : 'a:not(.skip-gallery)',
            type                : 'image',
            closeMarkup         : '<button title="%title%" type="button" class="modal-close js-modal-close"><svg class="i" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M18.7071 5.29289C19.0976 5.68342 19.0976 6.31658 18.7071 6.70711L6.70711 18.7071C6.31658 19.0976 5.68342 19.0976 5.29289 18.7071C4.90237 18.3166 4.90237 17.6834 5.29289 17.2929L17.2929 5.29289C17.6834 4.90237 18.3166 4.90237 18.7071 5.29289Z"/><path fill-rule="evenodd" clip-rule="evenodd" d="M5.29289 5.29289C5.68342 4.90237 6.31658 4.90237 6.70711 5.29289L18.7071 17.2929C19.0976 17.6834 19.0976 18.3166 18.7071 18.7071C18.3166 19.0976 17.6834 19.0976 17.2929 18.7071L5.29289 6.70711C4.90237 6.31658 4.90237 5.68342 5.29289 5.29289Z"/></svg></button>',
            closeOnContentClick : false,
            closeBtnInside      : true,
            mainClass           : 'mfp-with-zoom mfp-img-mobile',
            image               : {
                verticalFit     : true,
                titleSrc        : function(item) {
                    return item.el.attr('title');
                }
            },
            gallery             : {
                enabled         : true,
                arrowMarkup     : '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"><svg class="i" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 31.49" xml:space="preserve"><path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/></svg></button>',
            },
            zoom                : {
                enabled         : false,
                duration        : 300,
                opener          : function(element) {
                    return element.find('img');
                }
            }
        });
    }
};
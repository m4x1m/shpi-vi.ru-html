class StickyHeader {
    constructor() {
        this.body                 = APP.body;
        this.header               = document.getElementById('js-website-header');
        this.stickyClass          = '_header-is-static';
        this.collapsedClass       = '_header-is-collapsed';
        this.partialVisibleClass  = '_header-is-partial-visible';
        this.defaultPosition      = 0;

        this.bindEvents();
    }

    getHeaderHeight() {
        return this.header.clientHeight;
    }
    getScrollDirection() {
        window.addEventListener('scroll', ()=>{
            this.detectDirection();
        });
    }
    detectScrollTop() {
        return window.pageYOffset;
    }
    detectPageBottom() {
        let bottom = false;
        if ((window.innerHeight + Math.ceil(window.pageYOffset)) >= document.body.scrollHeight) {
            bottom = true
        }
        return bottom;
    }

    detectDirection() {
        const offset = this.detectScrollTop();

        //scroll up
        if ( offset <= this.defaultPosition ) {
            if ( window.deviceWidth <= WIDTH._991 ) {
                if ( this.defaultPosition - offset < 15 ) {
                    return false;
                }
            }

            if ( offset < this.getHeaderHeight() && offset >= 10 && !this.body.classList.contains(this.partialVisibleClass) ) {
                this.header.style.top = `-${this.detectScrollTop()}px`;
            }
            else if ( offset <= 10 ) {
                this.getHeaderStatic();
            }
            else if ( this.detectPageBottom() ) {
                this.getHeaderCollapsed();
            }
            else {
                this.getHeaderPartialVisible();
            }
        }
        //scroll down
        else {
            this.getHeaderCollapsed();
            if ( offset <= 0 && this.body.classList.contains(this.collapsedClass) ) {
                this.getHeaderStatic();
            }
        }
        this.defaultPosition = offset;
    }
    getHeaderCollapsed() {
        if ( this.detectScrollTop() < this.getHeaderHeight() ) {
            this.header.style.top = `-${this.detectScrollTop()}px`;
        }
        else {
            this.body.classList.add(this.collapsedClass);
            if ( this.body.classList.contains(this.partialVisibleClass) ) {
                this.body.classList.remove(this.partialVisibleClass);
            }
        }
    }
    getHeaderPartialVisible() {
        this.header.removeAttribute('style');
        this.body.classList.add(this.partialVisibleClass);
    }
    getHeaderSticky() {
        this.body.classList.add(this.collapsedClass);
    }
    getHeaderStatic() {
        this.body.classList.remove(this.collapsedClass, this.partialVisibleClass);
        this.header.removeAttribute('style');
    }
    resetHeader() {
        if ( window.pageYOffset <= 0 || window.pageYOffset <= 5 ) {
            this.body.classList.remove(this.collapsedClass, this.partialVisibleClass);
        }
    }
    bindEvents() {
        this.getScrollDirection();
    }
}
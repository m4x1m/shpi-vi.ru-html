const toggleOptionsHandler = (control, option) => {
    const PARAMS = {
        options     : document.querySelectorAll(control),
        option      : document.querySelectorAll(option),
        handler(element) {
            const data = element.getAttribute('data-option');
            const dataAdditional = element.getAttribute('data-additional-option');

            PARAMS.option.forEach(option => {
                option.hidden = true;
            });
            if ( data !== null ) {
                document.getElementById(data).hidden = false;
            }
            if ( dataAdditional !== null ) {
                document.getElementById(dataAdditional).hidden = false;
            }
        },
    };
    PARAMS.options.forEach(element => {
        element.addEventListener('change', ()=>{
            PARAMS.handler(element);
        });
    });
};
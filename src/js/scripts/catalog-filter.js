class CatalogFilter {
    constructor() {
        this.filter = document.querySelector('.js-catalog-filter');
        if ( this.filter === null )
            return false;

        this.form = this.filter.querySelector('.js-catalog-filter-form');

        this.button         = document.querySelector('.js-catalog-filter-button');
        this.close          = document.querySelector('.js-catalog-filter-close');
        this.expandedClass  = '_catalog-filtered';
        this.scrollPosition = 0;

        this.allowClickOutside = false;

        this.init();
    }

    currentScrollPosition() {
        return window.pageYOffset;
    }
    addClass(){
        APP.body.classList.add(this.expandedClass);
    }
    removeClass(){
        APP.body.classList.remove(this.expandedClass);
    }
    handler(){
        if ( !APP.body.classList.contains(this.expandedClass) ) {
            this.addClass();
            this.allowClickOutside = true;

            if ( window.deviceWidth <= WIDTH._767 ) {
                this.scrollPosition = this.currentScrollPosition();
            }
        }
        else {
            this.removeClass();
            this.allowClickOutside = false;

            if ( window.deviceWidth <= WIDTH._767 ) {
                window.scrollBy(0, this.scrollPosition);
            }
        }
    }
    closeHandler(){
        if ( APP.body.classList.contains(this.expandedClass) ) {
            this.removeClass();
            this.form.scrollTop = 0;
            this.allowClickOutside = false;

            if ( window.deviceWidth <= WIDTH._767 ) {
                window.scrollBy(0, this.scrollPosition);
            }
        }
    }
    bindEvents() {
        this.button.addEventListener('click', ()=>{
            this.handler();
        });
        this.close.addEventListener('click', ()=>{
            this.closeHandler();
        });

        document.addEventListener('click', (e)=>{
            if ( window.deviceWidth <= WIDTH._767 ) {
                let isClickInside = this.filter.contains(e.target);
                if (!isClickInside && this.allowClickOutside) {
                    this.closeHandler();
                }
            }
        });
    }
    init() {
        this.bindEvents();
    }
}
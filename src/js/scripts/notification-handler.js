const notificationHandler = (buttonClass, activeClass, func) => {
    const PARAMS = {
        body              : APP.body,
        button            : document.querySelector(buttonClass),
        notificationClass : activeClass,
        handler(){
            if ( PARAMS.body.classList.contains(PARAMS.notificationClass) ) {
                PARAMS.body.classList.remove(PARAMS.notificationClass);
            }
        }
    };

    if ( PARAMS.button !== null ) {
        PARAMS.button.addEventListener('click', ()=>{
            PARAMS.handler();
            func();
        });
    }
};
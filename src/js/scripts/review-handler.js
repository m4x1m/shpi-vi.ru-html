class ReviewHandler {
    constructor() {
        this.reviews = document.getElementsByClassName('js-reviews-add');
        if (this.reviews.length === 0)
            return false;

        this.favClass    = '.js-reviews-add-fav';
        this.activeClass = 'reviews-add--active';
        this.hiddenClass = 'form__row--hidden';

        this.init();
    }
    toggleFavouriteControl(element, favourite, event){
        const value = event.target.value;
        if ( value !== '' && !element.classList.contains( this.activeClass ) ) {
            element.classList.add( this.activeClass );
            favourite.classList.remove( this.hiddenClass );
        }
        else if ( value === '' && element.classList.contains( this.activeClass ) ) {
            element.classList.remove( this.activeClass );
            favourite.classList.add( this.hiddenClass );
        }
    }
    bindEvent(){
        const reviews = [].slice.call(this.reviews);

        reviews.forEach(element => {
            const textarea  = element.querySelector('textarea');
            const favourite = element.querySelector(this.favClass);

            textarea.addEventListener('keyup', (e)=>{
                this.toggleFavouriteControl(element, favourite, e);
            });
        });
    }
    init(){
        this.bindEvent();
    }
}
const priceRangeHandler = () => {
    if ( $('.js-price-range').length ) {
        $('.js-price-range').each(function(){
            const PARAMS = {
                range     : $(this),
                stepValue : parseInt($(this).attr('data-step-value')),
                minValue  : parseInt($(this).attr('data-min-value')),
                maxValue  : parseInt($(this).attr('data-max-value')),
                fromValue : parseInt($(this).attr('data-from-value')),
                toValue   : parseInt($(this).attr('data-to-value')),
                from      : $(this).parents('.js-price-range-block').find('.js-price-range-from'),
                to        : $(this).parents('.js-price-range-block').find('.js-price-range-to'),

                updateHandler(element, type){
                    const range = PARAMS.range.data("ionRangeSlider");
                    if ( type === 'from' ) {
                        range.update({
                            from: element.val()
                        });
                    }
                    else {
                        range.update({
                            to: element.val()
                        });
                    }
                },
                valuePrettifyHandler(element){
                    const value = parseInt(element.val());
                    element.val(value.toLocaleString('ru'));
                }
            };

            PARAMS.range.ionRangeSlider({
                skin         : 'round',
                type         : 'double',
                step         : PARAMS.stepValue,
                min          : PARAMS.minValue,
                max          : PARAMS.maxValue,
                from         : PARAMS.fromValue,
                to           : PARAMS.toValue,
                hide_min_max : true,
                hide_from_to : true,

                onStart: function(data) {
                    PARAMS.from.val(data.from.toLocaleString('ru'));
                    PARAMS.to.val(data.to.toLocaleString('ru'));
                },
                onChange: function(data) {
                    PARAMS.from.val(data.from.toLocaleString('ru'));
                    PARAMS.to.val(data.to.toLocaleString('ru'));
                },
                onFinish: function(data) {
                    PARAMS.from.trigger('keyup');
                },
                onUpdate: function(data) {}
            });
            PARAMS.from.on('change', function(){
                PARAMS.updateHandler($(this), 'from');
                PARAMS.valuePrettifyHandler($(this));
            });
            PARAMS.to.on('change', function(){
                PARAMS.updateHandler($(this), 'to');
                PARAMS.valuePrettifyHandler($(this));
            });
        });
    }
};
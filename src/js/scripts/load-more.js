const loadMore = () => {
    const PARAMS = {
        parent        : 'js-load-more',
        element       : '.js-load-more-element',
        buttons       : document.getElementsByClassName('js-load-more-button'),
        expandedClass : 'load-more--expanded',
        hiddenClass   : 'load-more__element--hidden',
        bindEvent : ()=> {
            for ( let button of PARAMS.buttons ) {
                button.addEventListener('click', ()=>{
                    const parent = PARAMS.findAncestor(button, PARAMS.parent);
                    const hideable = parent.getAttribute('data-hideable');

                    PARAMS.toggleHandler(parent, button, hideable);
                });
            }
        },
        toggleHandler : (parent, button, hideable)=> {
            const elements = parent.querySelectorAll(PARAMS.element);
            if ( hideable !== null ) {
                if ( !parent.classList.contains(PARAMS.expandedClass) ) {
                    parent.classList.add(PARAMS.expandedClass);
                    PARAMS.changeTextHandler(button, button.getAttribute('data-expanded'));

                    elements.forEach(element => element.classList.remove(PARAMS.hiddenClass));
                }
                else {
                    parent.classList.remove(PARAMS.expandedClass);
                    PARAMS.changeTextHandler(button, button.getAttribute('data-collapsed'));

                    elements.forEach(element => element.classList.add(PARAMS.hiddenClass));
                }
            }
            else {
                elements.forEach(element => element.classList.remove(PARAMS.hiddenClass));
                button.setAttribute('hidden', true);
            }
        },
        changeTextHandler(element, text){
            element.innerHTML = text;
        },
        findAncestor : (el, cls) => {
            while ((el = el.parentElement) && !el.classList.contains(cls));
            return el;
        }
    };

    PARAMS.bindEvent();
};

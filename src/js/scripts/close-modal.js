const closeModal = () => {
    $(document).on('click', '.js-modal-close', ()=>{
        $.magnificPopup.close();
    });
};
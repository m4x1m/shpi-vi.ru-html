const ReviewsComponent = () => {
    const PARAMS = {
        componentsClass   : '.js-reviews-component',
        components        : [...document.querySelectorAll('.js-reviews-component')],
        controlClass      : '.js-reviews-component-control',
        expandedClass     : 'reviews-component--expanded',
        inputClass        : '.js-input',
        inputFieldClass   : '.js-input-field',
        inputFocusedClass : 'input--focused',

        ExpandHandler(component){
            component.classList.add(PARAMS.expandedClass);
        },
        CollapseHandler(component){
            component.classList.remove(PARAMS.expandedClass);
        },
        CollapseAllHandler(){
            if ( document.querySelector(`${PARAMS.componentsClass}.${PARAMS.expandedClass}`) !== null ) {
                this.CollapseHandler(document.querySelector(`${PARAMS.componentsClass}.${this.expandedClass}`));
            }
        },
        SetUserNameHandler(input, name){
            const field =  input.querySelector(PARAMS.inputFieldClass);

            input.classList.add(PARAMS.inputFocusedClass);
            field.value = name + ', ';
            setTimeout(()=>{
                field.focus();
                field.selectionStart = field.selectionEnd = 10000;
            }, 200);
        },
        BindEvents(){
            PARAMS.components.forEach(component => {
                PARAMS.controls = component.querySelectorAll(PARAMS.controlClass);
                const input     = component.querySelector(PARAMS.inputClass);

                PARAMS.controls.forEach(control => {
                    control.addEventListener('click', (e)=>{
                        e.preventDefault();
                        const author = control.getAttribute('data-author');

                        if ( !component.classList.contains(PARAMS.expandedClass) ) {
                            PARAMS.CollapseAllHandler();
                            PARAMS.ExpandHandler(component);
                            PARAMS.SetUserNameHandler(input, author);
                        }
                        else {
                            PARAMS.SetUserNameHandler(input, author);
                        }
                    });
                });
            });
        },
        Init(){
            PARAMS.BindEvents();
        },
    };

    PARAMS.Init();
};
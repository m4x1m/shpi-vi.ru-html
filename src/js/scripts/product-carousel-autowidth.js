const ProductCarouselAutowidth = () => {
    const PARAMS = {
        carousel        : [...document.querySelectorAll('.js-product-carousel-autowidth')],
        arrowPrevClass  : '.js-product-carousel-autowidth-prev',
        arrowNextClass  : '.js-product-carousel-autowidth-next',
        pagerClass      : '.js-product-carousel-autowidth-pagination',
        slideClass      : '.swiper-slide'
    };

    PARAMS.carousel.forEach((element, i) =>{
        PARAMS.slides  = element.querySelectorAll(PARAMS.slideClass).length;
        PARAMS.prevArr = element.querySelector(PARAMS.arrowPrevClass);
        PARAMS.nextArr = element.querySelector(PARAMS.arrowNextClass);
        PARAMS.pager   = element.querySelector(PARAMS.pagerClass);

        let carouselArr = [];

        if ( PARAMS.slides > 1 ) {
            carouselArr[i] = new Swiper(element, {
                slidesPerView: 'auto',
                spaceBetween: 8,
                loop: false,
                navigation: {
                    prevEl: PARAMS.prevArr,
                    nextEl: PARAMS.nextArr,
                },
                pagination: {
                    el: PARAMS.pager,
                    clickable: true
                },
                breakpoints: {
                    768 : {
                        spaceBetween: 12
                    },
                    992 : {
                        spaceBetween: 20
                    },
                    1200 : {
                        spaceBetween: 20
                    },
                },
                on : {}
            });
        }
    });
};
const inputTel = () => {
    const PARAMS = {
        phone : $('.js-input-tel'),
    };

    if ( PARAMS.phone.length ) {
        PARAMS.phone.each(function(){
            const $this = $(this);

            $this.inputmask({
                mask            : '[+][9] 999 999-99-99',
                showMaskOnHover : false,
                clearIncomplete : true,
                onKeyValidation: function (key, result) {
                    if ( result.pos === 0 ) {
                        if ( key === 43 || key === 55 || key === 56 ) {
                            $this.val('+7');
                        }
                        else if ( key === 57 ) {
                            $this.val('+7 9');
                        }
                        else {
                            $this.val('');
                        }
                    }
                    if ( result.pos === 1 ) {
                        if ( key === 57 ) {
                            $this.val('+7 9');
                        }
                        else if ( key != 55 ) {
                            $this.val('+7');
                        }
                    }
                },
                onKeyDown: function ( event, buffer, caretPos, opts ) {
                    if ( caretPos.end === 2 && event.keyCode === 8 ) {
                        $this.val('');
                    }
                },
                onBeforeMask : function(value) {
                    //console.log(parseInt(value));
                    if ( iOSHandler() ) {
                        if ( value.length > 4 ) {
                            const reverseVal = value.split('').reverse().join('');
                            let slicedVal;
                            if ( value.length === 11 ) {
                                slicedVal = reverseVal.slice(0, -1);
                            }
                            else if ( value.length === 12 ) {
                                slicedVal = reverseVal.slice(0, -2);
                            }
                            else {
                                slicedVal = reverseVal;
                            }
                            const reversedVal = slicedVal.split('').reverse().join('');
                            const formattedValue = '+7'+reversedVal;

                            return formattedValue;
                        }
                    }
                }
            });
        });
    }
};
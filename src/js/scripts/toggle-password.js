const togglePassword = () => {
    const PARAMS = {
        parentClass : 'js-input',
        inputClass  : '.js-input-field-password',
        button      : document.querySelectorAll('.js-password-trigger'),
        activeClass : 'input--password-visible',
        handler(parent, input) {
            if ( !parent.classList.contains(PARAMS.activeClass) ) {
                parent.classList.add(PARAMS.activeClass);
                input.type = 'text';
            }
            else {
                parent.classList.remove(PARAMS.activeClass);
                input.type = 'password';
            }
        }
    };

    PARAMS.button.forEach(element => {
        const parent = findAncestor(element, PARAMS.parentClass);
        const input  = parent.querySelector(PARAMS.inputClass);

        element.addEventListener('click', ()=>{
            PARAMS.handler(parent, input);
        });
    });
};
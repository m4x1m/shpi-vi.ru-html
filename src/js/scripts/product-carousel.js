const ProductCarousel = () => {
    const PARAMS = {
        carousel        : [...document.querySelectorAll('.js-product-carousel')],
        arrowPrevClass  : '.js-product-carousel-prev',
        arrowNextClass  : '.js-product-carousel-next',
        pagerClass      : '.js-product-carousel-pagination',
        slideClass      : '.swiper-slide'
    };

    PARAMS.carousel.forEach((element, i) =>{
        PARAMS.slides  = element.querySelectorAll(PARAMS.slideClass).length;
        PARAMS.prevArr = element.querySelector(PARAMS.arrowPrevClass);
        PARAMS.nextArr = element.querySelector(PARAMS.arrowNextClass);
        PARAMS.pager   = element.querySelector(PARAMS.pagerClass);

        let carouselArr = [];
        let slidesCount;
        if ( window.deviceWidth < 767 ) {
            slidesCount = 2;
        }
        else if ( window.deviceWidth >= 768 && window.deviceWidth <= 1199 ) {
            slidesCount = 3;
        }
        else if ( window.deviceWidth > 1199 ) {
            slidesCount = 4;
        }

        if ( PARAMS.slides > slidesCount ) {
            carouselArr[i] = new Swiper(element, {
                slidesPerView: 'auto',
                spaceBetween: 0,
                loop: false,
                navigation: {
                    prevEl: PARAMS.prevArr,
                    nextEl: PARAMS.nextArr,
                },
                pagination: {
                    el: PARAMS.pager,
                    clickable: true
                },
                breakpoints: {
                    768 : {
                        slidesPerView: 3,
                        spaceBetween: 12
                    },
                    992 : {
                        slidesPerView: 3,
                        spaceBetween: 20
                    },
                    1200 : {
                        slidesPerView: 4,
                        spaceBetween: 20
                    },
                },
                on : {}
            });
        }
    });
};
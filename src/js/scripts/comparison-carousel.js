const comparisonCarousel = () => {
    const PARAMS = {
        slider  : $('.js-comparison-carousel'),
        props   : $('.js-comparison-carousel-props'),
        section : '.js-comparison-section'
    };

    if ( PARAMS.slider.length && PARAMS.props.length ) {

        PARAMS.slider.on('init', function(event, slick){
            $(this).removeClass(APP.hiddenCarousel);
        });
        PARAMS.props.on('init', function(event, slick){
            const parent = $(this).parent(PARAMS.slider);
            parent.removeClass(APP.hiddenCarousel);
        });

        PARAMS.slider.slick({
            asNavFor        : PARAMS.props,
            slidesToShow    : 4,
            variableWidth   : true,
            arrows          : true,
            prevArrow       : '<button type="button" class="slick-prev"><svg class="i" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.494 31.494" xml:space="preserve"><path d="M10.273,5.009c0.444-0.444,1.143-0.444,1.587,0c0.429,0.429,0.429,1.143,0,1.571l-8.047,8.047h26.554c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H3.813l8.047,8.032c0.429,0.444,0.429,1.159,0,1.587c-0.444,0.444-1.143,0.444-1.587,0l-9.952-9.952c-0.429-0.429-0.429-1.143,0-1.571L10.273,5.009z"/></svg></button>',
            nextArrow       : '<button type="button" class="slick-next"><svg class="i" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31.49 31.49" xml:space="preserve"><path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z"/></svg></button>',
            dots            : false,
            infinite        : false,
            touchThreshold  : 20,
            draggable       : false,
            swipeToSlide    : true,
            waitForAnimate  : false,
            responsive      : [
                {
                    breakpoint: 1450,
                    settings: {
                        slidesToShow   : 3
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow   : 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        variableWidth  : false,
                        slidesToShow   : 2,
                        arrows         : false
                    }
                }
            ]
        });
        PARAMS.props.slick({
            asNavFor        : PARAMS.slider,
            variableWidth   : true,
            slidesToShow    : 4,
            arrows          : false,
            dots            : false,
            infinite        : false,
            swipe           : false,
            draggable       : false,
            waitForAnimate  : false,
            responsive      : [
                {
                    breakpoint: 1450,
                    settings: {
                        slidesToShow   : 3
                    }
                },
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow   : 2
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        variableWidth  : false,
                        slidesToShow   : 2,
                        arrows         : false
                    }
                },
            ]
        });
    }
};
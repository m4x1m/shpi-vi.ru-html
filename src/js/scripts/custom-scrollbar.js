function CustomScrollbar(cont){
    this.cont = cont;
    this.scrollBars = [];
    this.init();
}
CustomScrollbar.prototype = {
    init: function(){
        const obj = this;
        if (this.cont.length > 0) {
            this.cont.each(function(){
                obj.scrollBars.push(new PerfectScrollbar(this));
            })
        }
    },
    update: function(){
        this.scrollBars.forEach(el =>{
            el.update();
        })
    }
};
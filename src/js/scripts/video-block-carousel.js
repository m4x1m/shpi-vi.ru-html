const videoBlockCarousel = () => {
    const PARAMS = {
        carousel : '.js-video-block-carousel',
        element  : '.swiper-slide'
    };
    if ( $(PARAMS.carousel).length && $(PARAMS.carousel).find(PARAMS.element).length > 1 ) {
        const carousel = new Swiper(PARAMS.carousel, {
            slidesPerView : 'auto',
            spaceBetween  : 8,
            loop: true,
            navigation: {
                nextEl: '.js-video-block-carousel-next',
            },
            pagination: {
                el : '.js-video-block-carousel-pagination',
                clickable : true
            },
            breakpoints: {
                768: {
                    slidesPerView : 1,
                    spaceBetween  : 0
                }
            },
        });
    }
};
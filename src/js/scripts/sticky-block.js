class StickyBlock {
    constructor() {
        this.block       = document.getElementById('js-sticky-block');

        if (this.block === null)
            return false;

        this.element     = this.block.querySelector('.js-sticky-element');
        this.stickyClass = 'sticky-element--sticky';
        this.offset      = 20;

        this.init();
    }

    detectOffsetTop() {
        return window.pageYOffset;
    }
    detectBlockTopPosition(){
        return this.block.offsetTop;
    }
    detectBlockLeftPosition(){
        return this.element.offsetLeft + 'px';
    }
    detectBlockWidth(){
        return this.element.clientWidth + 'px';
    }
    detectBlockHeight() {
        return this.element.clientHeight;
    }
    detectBlockBottomPosition() {
        return this.detectBlockTopPosition() + this.block.clientHeight - this.detectBlockHeight() - this.offset;
    }
    detectStickyBlockPosition() {
        return this.element.offsetTop;
    }

    stickyHandler(){
        if ( this.detectOffsetTop() >= this.detectStickyBlockPosition()
            && this.detectOffsetTop() <= this.detectBlockBottomPosition() )
        {
            if ( !this.element.classList.contains(this.stickyClass) ) {
                this.element.style.cssText  = `left: ${this.detectBlockLeftPosition()}; width: ${this.detectBlockWidth()}`;
                this.element.classList.add(this.stickyClass);
            }
        }
        else {
            if ( this.element.classList.contains(this.stickyClass) ) {
                this.element.removeAttribute('style');
                this.element.classList.remove(this.stickyClass);
            }
        }
    }
    bindEvent() {
        window.addEventListener('scroll',()=>{
            this.stickyHandler();
        });
    }
    init() {
        if ( window.deviceWidth > WIDTH._991 ) {
            this.bindEvent();
        }
    }
}
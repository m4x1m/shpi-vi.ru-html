const BrandsCarousel = () => {
    const PARAMS = {
        carousel        : [...document.querySelectorAll('.js-brands-carousel')],
        arrowPrevClass  : '.js-brands-carousel-prev',
        arrowNextClass  : '.js-brands-carousel-next',
        slideClass      : '.swiper-slide'
    };

    PARAMS.carousel.forEach((element, i) =>{
        PARAMS.slides  = element.querySelectorAll(PARAMS.slideClass).length;
        PARAMS.prevArr = element.querySelector(PARAMS.arrowPrevClass);
        PARAMS.nextArr = element.querySelector(PARAMS.arrowNextClass);

        let carouselArr = [];

        if ( PARAMS.slides > 6 ) {
            carouselArr[i] = new Swiper(element, {
                slidesPerView: 'auto',
                spaceBetween: 8,
                loop: true,
                navigation: {
                    prevEl: PARAMS.prevArr,
                    nextEl: PARAMS.nextArr,
                },
                pagination: {},
                breakpoints: {
                    768 : {
                        spaceBetween: 20
                    },
                    992 : {
                        spaceBetween: 40
                    },
                },
                on : {}
            });
        }
    });
};
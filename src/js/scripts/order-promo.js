const orderPromoHandler = () => {
    const PARAMS = {
        promo         : 'js-order-promo',
        input         : document.getElementById('js-order-promo-input'),
        notEmptyClass : '_not-empty',
        handler(element) {
            const parent = findAncestor(element, PARAMS.promo);
            if ( element.value != '' ) {
                parent.classList.add(PARAMS.notEmptyClass);
            }
            else {
                parent.classList.remove(PARAMS.notEmptyClass);
            }
        }
    };

    if ( PARAMS.input ) {
        PARAMS.input.addEventListener('keyup', (e)=>{
            PARAMS.handler(e.target);
        });
    }
};
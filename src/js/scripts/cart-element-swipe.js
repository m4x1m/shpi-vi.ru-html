const cartElementSwipeHandler = () => {
    const PARAMS = {
        allowSwipe  : (window.deviceWidth <= WIDTH._767 ? true : false),
        swipeStart  : parseInt($(window).width() * (66 / 100)),
        swipeMiddle : parseInt($(window).width() * (50 / 100)),
        swipeEnd    : parseInt($(window).width()),
        distance    : 150,
    };
    const CART = {
        element : '.js-cart-swipe-element',
        image   : '.js-cart-swipe-element-image',
        details : '.js-cart-swipe-element-details',
        readyClass : 'cart-element--ready-to-be-removed',

        removeAttrHandler(data) {
            data.image.removeAttr('style');
            data.details.removeAttr('style');
        }
    };

    if ( PARAMS.allowSwipe && $(CART.element).length ) {
        $(CART.element).swipe({
            threshold        : 50,
            maxTimeThreshold : 1000,
            fingers          : 1,
            allowPageScroll  : 'vertical',
            excludedElements :'.js-no-swipe',
            swipeStatus:function(event, phase, direction, distance, duration, fingers, fingerData, currentDirection) {
                const parent  = $(event.target).parents(CART.element);
                const image   = parent.find(CART.image);
                const details = parent.find(CART.details);
                //mark to remove
                if ( fingerData[0].start.x >= PARAMS.swipeStart && fingerData[0].end.x <= PARAMS.swipeEnd ) {
                    if ( phase === 'cancel' || direction === 'right' && !parent.hasClass(CART.readyClass) ) {
                        CART.removeAttrHandler({image : image, details : details});
                    }
                    if ( direction === 'left' ) {
                        if ( phase === 'end' ) {
                            if ( distance <= PARAMS.distance ) {
                                CART.removeAttrHandler({image : image, details : details});
                            }
                            else {
                                parent.addClass(CART.readyClass);
                                CART.removeAttrHandler({image : image, details : details});
                            }
                        }
                        if ( phase === 'move' ) {
                            image.css('transform', `translateX(-${distance / 3}px)`);
                            details.css('transform', `translateX(-${distance / 3}px)`);
                        }
                        if ( parent.hasClass(CART.readyClass) ) {
                            return false;
                        }
                    }
                    if ( direction === 'up' || direction === 'down' ) {
                        return false;
                    }
                }
                //unmark not to remove
                if ( fingerData[0].start.x >= PARAMS.swipeMiddle && fingerData[0].end.x <= PARAMS.swipeEnd ) {
                    if ( direction === 'right' && parent.hasClass(CART.readyClass) ) {
                        if ( phase === 'end' ) {
                            if ( distance <= PARAMS.distance ) {
                                parent.removeClass(CART.readyClass);
                            }
                        }
                    }
                }
            }
        });
    }
};
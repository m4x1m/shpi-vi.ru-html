const preventPageScroll = () => {
    const PARAMS = {
        block : document.querySelectorAll('.js-scroll-prevent'),
        preventDefault : (e)=>{
            e.preventDefault();
        }
    };
    const array = [...PARAMS.block];

    array.forEach(element => {
        element.addEventListener('DOMMouseScroll',(e)=>{
            PARAMS.preventDefault(e);
        },); // {passive: true}
        element.addEventListener('mousewheel',(e)=>{
            PARAMS.preventDefault(e);
        });
        element.addEventListener('touchmove',(e)=>{
            PARAMS.preventDefault(e);
        });
    });
};
class VerificationCode {
    constructor() {
        this.input       = document.getElementById('js-auth-code-verification-input');

        if (this.input === null)
            return false;

        this.marker      = '.js-auth-code-verification-marker';
        this.markers     = document.querySelectorAll(this.marker);
        this.activeClass = '_active';

        this.init();
    }
    addClass(element) {
        if ( !element.classList.contains(this.activeClass) ) {
            element.classList.add(this.activeClass);
        }
    }
    removeClass(element){
        if ( element.classList.contains(this.activeClass) ) {
            element.classList.remove(this.activeClass);
        }
    }
    checkActive(length) {
        let i = 0;
        const markersLength = this.markers.length;

        for ( i; markersLength > i ; i++ ) {
            if ( i <= length ) {
                this.addClass(this.markers[i]);
            }
            else {
                this.removeClass(this.markers[i]);
            }
        }
    }
    toggleActive(input, e){
        const inputLength = input.value.length;
        this.checkActive(inputLength);

        if ( inputLength >= 4 ) {
            input.value = input.value.slice(0, 4);
            input.blur();
        }
    }
    bindEvents() {
        this.input.addEventListener('keyup', (e)=>{
            setInputFilter(e.target, (value)=> {
                return /^-?\d*$/.test(value);
            });
            this.toggleActive(e.target, e);
        });
        this.input.addEventListener('focus', ()=>{
            if (this.input.value <= 0 ) {
                this.addClass(this.markers[0])
            }
        });
        this.input.addEventListener('blur', ()=>{
            if (this.input.value <= 0 ) {
                this.removeClass(this.markers[0]);
            }
        });
    }
    init(){
        this.bindEvents();
    }
}
const setWidth = () => {
    APP.websiteHeader.style.maxWidth = window.deviceWidth + 'px';
    APP.websiteWrapper.style.maxWidth = window.deviceWidth + 'px';
};
const unsetWidth = () => {
    APP.websiteHeader.style.maxWidth = '';
    APP.websiteWrapper.style.maxWidth = '';
};

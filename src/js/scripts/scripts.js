if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}
window.deviceWidth = document.querySelector('html').clientWidth;
window.addEventListener("resize", (e)=> {
    const updatedWidth = document.querySelector('html').clientWidth;

    if ( window.deviceWidth != updatedWidth  ) {
        window.deviceWidth = updatedWidth;
    }
});

//function init
iosDetect();
loadMore();
quickSearch();
new StickyHeader();
new StickyNavigation();
new WebsiteAside();
new ScrollableBlock();
new ShowMore();
new CollapsibleSection();
new Counter();
new InputHandler();
//new StickyBlock();
//new Accordion('.js-accordions-collapsible', true);
new VerificationCode();
new RatingControl();
new ReviewHandler();
new CatalogFilter();
//new IssueFeedback();
new FAQComponent();

preventPageScroll();
orderPromoHandler();
textAreaAutoHeight();
toggleOptionsHandler('.js-payment-control', '.js-payment-option');
toggleOptionsHandler('.js-delivery-control', '.js-delivery-option');
toggleOptionsHandler('.js-order-type-control', '.js-order-type');
togglePassword();
websiteOptionsNav();
toggleBlock();
messengers();
ReviewsComponent();
//notificationHandler('.js-notification-button', '_notification-is-opened', someFunc);
//notificationHandler('.js-warning-button', '_warning-is-opened', someFunc);

$(document).ready(()=>{
    defaultSlider();
    tooltip(WIDTH._767);
    accordion('.js-accordions-collapsible', true);
    accordion('.js-accordions-non-collapsible', false);
    vThumbsGallery();
    hThumbsGallery();
    mobileCarousel();
    videoBlockCarousel();
    formValidate();
    selectStyler();
    inputTel();
    comparisonCarousel();
    yandexMapInit();
    modal(APP.modalTrigger);
    photoGallery($('.js-photo-gallery'));
    videoModal();
    closeModal();
    priceRangeHandler();
    cartElementSwipeHandler();
    topLinkHandler();
    ProductCarousel();
    ProductCarouselAutowidth();
    BrandsCarousel();

    new Tabs();
});
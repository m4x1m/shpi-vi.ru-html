const quickSearch = () => {
    const PARAMS = {
        parent            : 'js-quick-search',
        input             : document.getElementsByClassName('js-quick-search-input')[0],
        reset             : document.getElementsByClassName('js-quick-search-reset')[0],
        button            : document.getElementById('js-quick-search-button'),
        barButton         : document.getElementById('js-quick-search-bar-button'),
        focusClass        : 'quick-search--focused',
        notEmptyClass     : 'quick-search--not-empty',
        expandedClass     : '_quick-search-is-expanded',
        buttonActiveClass : '_active',

        mobileNavButton   : document.querySelector('.js-website-aside-button'),
        mobileActiveClass : '_website-aside-is-opened'
    };
    let allowClickOutside = false;
    const init = () => {
        const parent = findAncestor(PARAMS.input, PARAMS.parent);
        PARAMS.input.addEventListener('focus',(e)=>{
            if ( !parent.classList.contains(PARAMS.focusClass) ) {
                parent.classList.add(PARAMS.focusClass)
            }
            if ( e.target.value != '' ) {
                parent.classList.add(PARAMS.notEmptyClass);
            }
        });
        PARAMS.input.addEventListener('blur', (e)=>{
            if ( parent.classList.contains(PARAMS.focusClass) && e.target.value === '' ) {
                parent.classList.remove(PARAMS.focusClass);
            }
        });
        PARAMS.input.addEventListener('keyup',(e)=>{
            if ( e.target.value != '' ) {
                parent.classList.add(PARAMS.notEmptyClass);
                allowClickOutside = true;
            }
            else {
                parent.classList.remove(PARAMS.notEmptyClass);
                allowClickOutside = false;
            }
        });
        //reset input
        PARAMS.reset.addEventListener('click',()=>{
            parent.classList.remove(PARAMS.focusClass);
            parent.classList.remove(PARAMS.notEmptyClass);
        });

        //mobile button
        PARAMS.button.addEventListener('click', (e)=>{
            if ( !APP.body.classList.contains(PARAMS.expandedClass) ) {
                e.target.classList.add(PARAMS.buttonActiveClass);
                APP.body.classList.add(PARAMS.expandedClass);

                focusAndOpenKeyboard(PARAMS.input, 150);
                setTimeout(()=>{
                    allowClickOutside = true
                }, 200);
            }
            else {
                APP.body.classList.remove(PARAMS.expandedClass);
                e.target.classList.remove(PARAMS.buttonActiveClass);
                allowClickOutside = false;
            }

            //hide mobile navigation
            if ( APP.body.classList.contains(PARAMS.mobileActiveClass) ) {
                PARAMS.mobileNavButton.click();
            }
        });
        PARAMS.barButton.addEventListener('click', (e)=>{
            e.preventDefault();
            PARAMS.button.click();
        });

        //hide dropdown on click outside
        document.addEventListener('click', (e)=>{
            let isClickInside = document.getElementById(PARAMS.parent).contains(e.target);
            if (!isClickInside && allowClickOutside) {
                parent.classList.remove(PARAMS.notEmptyClass);
                //for mobile search
                if ( APP.body.classList.contains(PARAMS.expandedClass) ) {
                    APP.body.classList.remove(PARAMS.expandedClass);
                    PARAMS.button.classList.remove(PARAMS.buttonActiveClass);
                    allowClickOutside = false;
                }
            }
        });
    };
    init();
};
// stackoverflow solution
const focusAndOpenKeyboard = (el, timeout) => {
    if(!timeout) {
        timeout = 100;
    }
    if(el) {
        // Align temp input element approximately where the input element is
        // so the cursor doesn't jump around
        var __tempEl__ = document.createElement('input');
        __tempEl__.style.position = 'absolute';
        __tempEl__.style.top = (el.offsetTop + 7) + 'px';
        __tempEl__.style.left = el.offsetLeft + 'px';
        __tempEl__.style.height = 0;
        __tempEl__.style.opacity = 0;
        // Put this temp element as a child of the page <body> and focus on it
        document.body.appendChild(__tempEl__);
        __tempEl__.focus();

        // The keyboard is open. Now do a delayed focus on the target element
        setTimeout(function() {
            el.focus();
            el.click();
            // Remove the temp element
            document.body.removeChild(__tempEl__);
        }, timeout);
    }
};
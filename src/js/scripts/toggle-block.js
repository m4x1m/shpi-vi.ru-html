const toggleBlock = () => {
    const PARAMS = {
        button : document.querySelectorAll('.js-toggle-block-button'),

        hiddenHandler(id) {
            document.getElementById(id).hidden = true;
        },
        visibilityHandler(id) {
            document.getElementById(id).hidden = false;
        },
        handler(element){
            const id = element.getAttribute('data-toggle-block');
            const selfHide = element.getAttribute('data-self-hide');

            if ( document.getElementById(id).getAttribute('hidden') != null ) {
                PARAMS.visibilityHandler(id);
                if ( selfHide !== null ) {
                    element.hidden = true;
                }
            }
            else {
                PARAMS.hiddenHandler(id);
            }
        }
    };

    PARAMS.button.forEach(element => {
        element.addEventListener('click', ()=>{
            PARAMS.handler(element);
        });
    });
};
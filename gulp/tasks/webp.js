'use strict';

import webp from 'imagemin-webp';
import extReplace from 'gulp-ext-replace';
import { src, dest } from 'gulp';
import { $, source, build } from '../config';

// Assets
const Webp = () => src(source.webp)
    .pipe($.plumber())
    .pipe($.changed(source.webp))
    .pipe($.imagemin(webp({quality: 75})))
    .pipe(extReplace(".webp"))
    .pipe(dest(build.webp));

export default Webp;


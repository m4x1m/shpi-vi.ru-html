'use strict';

import { src, dest } from 'gulp';
import { production, nominify, $, source, build, config } from '../config';

// MODULES JS
const modulesJs = () => src(source.modulesJs)
    .pipe($.plumber())
    .pipe($.if(!production, $.sourcemaps.init()))
    .pipe($.include(config.include)).on('error', (e) => console.log(e))
    .pipe($.babel(config.babel))
    .pipe($.if(production && !nominify, $.uglifyes()))
    .pipe(($.if(!production, $.sourcemaps.write('.'))))
    .pipe(dest(build.scripts))

export default modulesJs;